<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Model\ProductUnit;
use App\Type\AttachmentType;
use App\Type\StatusType;
use App\Model\ProductAttachment;

class Product extends BaseModel 
{
    protected $table = 'tbl_product';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }

    public function productUnit() {
        return $this->belongsTo('App\Model\ProductUnit','product_unit_id');
    }

    public static function Count(){
        return Product::all()->where('status', '=',StatusType::Active)->count();
    }

    public function product_attachments() {
        return $this->hasMany(ProductAttachment::class, 'product_id');
    }

    public function product_photo(){
        $product_photo=$this->product_attachments->where('attachment_types_id', AttachmentType::ProductPhoto)->first();

        if($product_photo != null){
            if(!Storage::exists('public/images/product_attachments/'.$product_photo->path)){
                $product_photo->path='add-product.png';
            }
        }else{
            $product_photo=new ProductAttachment();
            $product_photo->path='add-product.png';
        }
        
        return $product_photo;
    }

}
