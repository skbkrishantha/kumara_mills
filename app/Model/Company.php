<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends BaseModel 
{
    protected $table = 'tbl_company';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    } 
}
