<?php

namespace App\Model;

class CashBook extends BaseModel 
{
    protected $table = 'tbl_cashbook';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;

    protected $dates = ['joined_date'];
    
    public static function boot()
    {
        parent::boot();
    }  
}
