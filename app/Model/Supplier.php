<?php

namespace App\Model;

use Illuminate\Support\Facades\Storage;
use App\Type\AttachmentType;
use App\Type\StatusType;

class Supplier extends BaseModel 
{
    protected $table = 'tbl_supplier';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;

    protected $dates = ['joined_date'];
    
    public static function boot()
    {
        parent::boot();
    }  
    
    public function supplier_attachments() {
        return $this->hasMany(SupplierAttachment::class, 'supplier_id');
    }

    public function supplier_photo(){
        $supplier_photo=$this->supplier_attachments->where('attachment_types_id', AttachmentType::SupplierPhoto)->first();

        if($supplier_photo != null){
            if(!Storage::exists('public/images/supplier_attachments/'.$supplier_photo->path)){
                $supplier_photo->path='avatar-user.png';
            }
        }else{
            $supplier_photo=new EmployeeAttachment();
            $supplier_photo->path='avatar-user.png';
        }
        
        return $supplier_photo;
    }

    public function getDisplayDetailsAttribute()
    {
        return $this->name . ' | ' . $this->identity_card_type_name().' | '.$this->identity_card_number;
    }

    public function getDisplayIdentityCardAttribute()
    {
        return $this->identity_card_type_name().' | '.$this->identity_card_number;
    }

    public function getJoinedDateFormatedAttribute()
    {
        if(isset($this->joined_date))
        {
            return $this->joined_date->format('Y-m-d');
        }else{
            return $this->joined_date; 
        }
    }

    public function identity_card_type() {
        return $this->belongsTo(IdentityCardType::class, 'identity_card_type_id');
    }

    public function identity_card_type_name() {
        if($this->identity_card_type != null)
        {
            $identity_card_type_name=$this->identity_card_type->display_name;
        }else{
            $identity_card_type_name= 'NONE';
        }
        return $identity_card_type_name;
    }

    public static function SupplierCount(){
        return Supplier::all()->where('status', '=',StatusType::Active)->count();
    }
}
