<?php

namespace App\Model;

class ClientAttachment extends BaseModel 
{
    protected $table = 'tbl_client_attachments';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }  
    
    public function attachment_type() {
        return $this->belongsTo(AttachmentType::class,'attachment_types_id');
    }
}
