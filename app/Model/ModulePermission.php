<?php

namespace App\Model;

class ModulePermission extends BaseModel
{
    protected $table = 'mst_module_permission';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;

    protected $fillable = [
        'id','name','dispaly_name','status',
    ];
    
    public static function boot()
    {
        parent::boot();
    } 
}
