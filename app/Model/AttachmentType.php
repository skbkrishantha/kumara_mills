<?php

namespace App\Model;

class AttachmentType extends BaseModel 
{
    protected $table = 'mst_attachment_types';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }      
}
