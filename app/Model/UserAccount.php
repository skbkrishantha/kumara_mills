<?php

namespace App\Model;
use App\Type\CommonType;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Traits\LockableTrait;

class UserAccount extends Authenticatable
{
    use Notifiable;
    use LockableTrait;

    protected $table = 'tbl_user_account';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','email','lockout_time','user_password','user_name','remember_token', 'status','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_password', 'remember_token',
    ];


    public function getAuthPassword()
    {
        return $this->user_password;
    }

    public function role() {
        return $this->belongsTo(Role::class,'role_id');
    }

    public function employee() {
        return $this->hasOne(Employee::class, 'user_account_id');
    }

    public function company(){
        return Company::find(CommonType::CompanyId);
    }
}
