<?php

namespace App\Model;

class Stock extends BaseModel 
{
    protected $table = 'tbl_stock';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;

    protected $dates = ['joined_date'];
    
    public static function boot()
    {
        parent::boot();
    }  
        
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}


