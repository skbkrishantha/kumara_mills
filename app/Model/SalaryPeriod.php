<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SalaryPeriod extends BaseModel 
{
    protected $table = 'mst_salary_periods';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }
}
