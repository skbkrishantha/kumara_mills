<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model{
   
     public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user(); 
            if($user != null) {                
                $model->insert_user_id = $user->id;
                $model->update_user_id = $user->id;
            }         
        });
        static::updating(function($model)
        {
            $user = Auth::user();
            if($user != null) {              
                $model->update_user_id = $user->id;
            }
        });       
    }

    public function insertBy(){
        if(UserAccount::find($this->insert_user_id) != null){            
            return UserAccount::find($this->insert_user_id)->employee->name;
        }else{
            return "System";
        }
    }

    public function insertTime(){           
        return $this->insert_datetime;
    }

    public function recordBy(){
        return 'Recorded By :: '.$this->insertBy().'@'.$this->insertTime();
    }
   
}