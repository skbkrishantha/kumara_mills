<?php

namespace App\Model;

class Event extends BaseModel
{
    protected $table = 'tbl_event';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    } 
}
