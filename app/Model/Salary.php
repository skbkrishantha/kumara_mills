<?php

namespace App\Model;

class Salary extends BaseModel
{
    protected $table = 'tbl_salary';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;

    protected $dates = ['date'];
    
    public static function boot()
    {
        parent::boot();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function salaryPeriod()
    {
        return $this->belongsTo(SalaryPeriod::class);
    }

    public function cashbook()
    {
        return $this->hasMany(CashBook::class,'reference_id');
    }

}
