<?php

namespace App\Model;

class SupplierAttachment extends BaseModel 
{
    protected $table = 'tbl_supplier_attachments';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }  
    
    public function attachment_type() {
        return $this->belongsTo(AttachmentType::class,'attachment_types_id');
    }
}
