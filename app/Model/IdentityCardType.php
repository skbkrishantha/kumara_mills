<?php

namespace App\Model;

class IdentityCardType extends BaseModel 
{
    protected $table = 'mst_identity_card_types';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }      
}
