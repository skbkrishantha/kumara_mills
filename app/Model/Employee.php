<?php

namespace App\Model;

use App\Type\AttachmentType;
use App\Type\StatusType;

use Illuminate\Support\Facades\Storage;

class Employee extends BaseModel 
{
    protected $table = 'tbl_employee';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;

    protected $dates = ['joined_date'];
    
    public static function boot()
    {
        parent::boot();
    } 

    public function user_account() {
        return $this->belongsTo(UserAccount::class, 'user_account_id');
    }
    
    public function employee_attachments() {
        return $this->hasMany(EmployeeAttachment::class, 'employee_id');
    }

    public function employee_photo(){
        $employee_photo=$this->employee_attachments->where('attachment_types_id', AttachmentType::EmployeePhoto)->first();

        if($employee_photo != null){
            if(!Storage::exists('public/images/employee_attachments/'.$employee_photo->path)){
                $employee_photo->path='avatar-user.png';
            }
        }else{
            $employee_photo=new EmployeeAttachment();
            $employee_photo->path='avatar-user.png';
        }
        
        return $employee_photo;
    }

    public function identity_card_type() {
        return $this->belongsTo(IdentityCardType::class, 'identity_card_type_id');
    }

    public function identity_card_type_name() {
        if($this->identity_card_type != null)
        {
            $identity_card_type_name=$this->identity_card_type->display_name;
        }else{
            $identity_card_type_name= 'NONE';
        }
        return $identity_card_type_name;
    }

    public function getDisplayIdentityCardAttribute()
    {
        return $this->identity_card_type_name().' | '.$this->identity_card_number;
    }

    public function getJoinedDateFormatedAttribute()
    {
        if(isset($this->joined_date))
        {
            return $this->joined_date->format('Y-m-d');
        }else{
            return $this->joined_date; 
        }
    }

    public static function Count(){
        return Employee::all()->where('status', '=',StatusType::Active)->count();
    }
}
