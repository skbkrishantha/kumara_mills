<?php

namespace App\Model;

class Sell extends BaseModel
{
    protected $table = 'tbl_sell';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;

    protected $dates = ['date'];
    
    public static function boot()
    {
        parent::boot();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function stock()
    {
        return $this->hasMany(Stock::class,'reference_id');
    }

    public function cashbook()
    {
        return $this->hasMany(CashBook::class,'reference_id');
    }

    public function subtotal(){
        $subtotal = 0;
        $stocks = $this->stock;
        foreach($stocks as $stock){
            $subtotal += ($stock->units * $stock->unit_price);
        }
        return $subtotal;
    }

    public function discount(){
        return ($this->subtotal() - $this->total());
    }

    public function total(){
        $total = 0;
        $cashbooks = $this->cashbook;
        foreach($cashbooks as $cashbook){
            $total += $cashbook->amount;
        }
        return $total;
    }
}
