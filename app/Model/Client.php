<?php

namespace App\Model;

use App\Type\AttachmentType;
use App\Type\StatusType;

use Illuminate\Support\Facades\Storage;

class Client extends BaseModel 
{
    protected $table = 'tbl_client';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;

    protected $dates = ['joined_date'];
    
    public static function boot()
    {
        parent::boot();
    } 

    public function client_attachments() {
        return $this->hasMany(ClientAttachment::class, 'client_id');
    }

    public function client_photo(){
        $client_photo=$this->client_attachments->where('attachment_types_id', AttachmentType::ClientPhoto)->first();

        if($client_photo != null){
            if(!Storage::exists('public/images/client_attachments/'.$client_photo->path)){
                $client_photo->path='avatar-user.png';
            }
        }else{
            $client_photo=new EmployeeAttachment();
            $client_photo->path='avatar-user.png';
        }
        
        return $client_photo;
    }

    public function getDisplayDetailsAttribute()
    {
        return $this->name . ' | ' . $this->identity_card_type_name().' | '.$this->identity_card_number;
    }

    public function getDisplayIdentityCardAttribute()
    {
        return $this->identity_card_type_name().' | '.$this->identity_card_number;
    }

    public function getJoinedDateFormatedAttribute()
    {
        if(isset($this->joined_date))
        {
            return $this->joined_date->format('Y-m-d');
        }else{
            return $this->joined_date; 
        }
    }

    public function identity_card_type() {
        return $this->belongsTo(IdentityCardType::class, 'identity_card_type_id');
    }

    public function identity_card_type_name() {
        if($this->identity_card_type != null)
        {
            $identity_card_type_name=$this->identity_card_type->display_name;
        }else{
            $identity_card_type_name= 'NONE';
        }
        return $identity_card_type_name;
    }

    public static function ClientCount(){
        return Client::all()->where('status', '=',StatusType::Active)->count();
    }

}
