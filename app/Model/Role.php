<?php

namespace App\Model;

use App\Util\CommonFunc;
use App\Type\StatusType;

class Role extends BaseModel 
{
    protected $table = 'mst_user_role';
    const CREATED_AT = 'INSERT_DATETIME';
    const UPDATED_AT = 'UPDATE_DATETIME';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','dispaly_name','status',
    ];
    
    public function permissions()
    {
        return $this->belongsToMany(ModulePermission::class,'tbl_user_role_module_permission','user_role_id','module_permission_id');
    }

    /*public function givePermissionTo(ModulePermission $permission)
    {
        return $this->permissions()->save($permission);
    }*/

    public function hasPermission(ModulePermission $permission)
    {
        return $this->permissions->contains($permission);
    }
}
