<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends BaseModel 
{
    protected $table = 'mst_product_units';
    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';
    public $incrementing = false;
    
    public static function boot()
    {
        parent::boot();
    }
}
