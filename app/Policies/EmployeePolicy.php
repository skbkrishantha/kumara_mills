<?php

namespace App\Policies;

use App\Model\UserAccount;
use App\Model\ModulePermission;
use App\Type\ModulePermissionType;
use App\Type\ModuleType;

use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{   
    /**
     * Determine whether the role can view the module.
     *
     * @param  \App\Model\UserAccount  $userAccount
     * @return mixed
     */
    public function view(UserAccount $userAccount)
    {
        $permission=new ModulePermission();
        if(ModulePermission::where('module_name',ModuleType::Employee)->count() > 0){
            $permission=ModulePermission::where('module_name',ModuleType::Employee )
                                        ->where('permission',ModulePermissionType::view)
                                        ->first();
            if($permission==null){
                $permission=new ModulePermission();
            }
        }
        return $userAccount->role->hasPermission($permission);
    }
    /**
     * Determine whether the userAccount can create module.
     *
     * @param  \App\Model\UserAccount  $userAccount
     * @return mixed
     */
    public function create(UserAccount $userAccount)
    {
        $permission=new ModulePermission();
        if(ModulePermission::where('module_name',ModuleType::Employee)->count() > 0){
            $permission=ModulePermission::where('module_name',ModuleType::Employee )
                                        ->where('permission',ModulePermissionType::create)
                                        ->first();
            if($permission==null){
                $permission=new ModulePermission();
            }
        }
        return $userAccount->role->hasPermission($permission);
    }
    /**
     * Determine whether the userAccount can update the module.
     *
     * @param  \App\Model\UserAccount  $userAccount
     * @return mixed
     */
    public function update(UserAccount $userAccount)
    {
        $permission=new ModulePermission();
        if(ModulePermission::where('module_name',ModuleType::Employee)->count() > 0){
            $permission=ModulePermission::where('module_name',ModuleType::Employee )
                                        ->where('permission',ModulePermissionType::update)
                                        ->first();
            if($permission==null){
                $permission=new ModulePermission();
            }
        }
        return $userAccount->role->hasPermission($permission);
    }
    /**
     * Determine whether the userAccount can delete the module.
     *
     * @param  \App\Model\UserAccount  $userAccount
     * @return mixed
     */
    public function delete(UserAccount $userAccount)
    {
        $permission=new ModulePermission();
        if(ModulePermission::where('module_name',ModuleType::Employee)->count() > 0){
            $permission=ModulePermission::where('module_name',ModuleType::Employee )
                                        ->where('permission',ModulePermissionType::delete)
                                        ->first();
            if($permission==null){
                $permission=new ModulePermission();
            }
        }
        return $userAccount->role->hasPermission($permission);
    }

    /**
     * Determine whether the userAccount can visible the module.
     *
     * @param  \App\Model\UserAccount  $userAccount
     * @return mixed
     */
    public function visible(UserAccount $userAccount)
    {
        $permission=new ModulePermission();
        if(ModulePermission::where('module_name',ModuleType::Employee)->count() > 0){
            $permission=ModulePermission::where('module_name',ModuleType::Employee )
                                        ->where('permission',ModulePermissionType::visible)
                                        ->first();
            if($permission==null){
                $permission=new ModulePermission();
            }
        }
        return $userAccount->role->hasPermission($permission);
    }
}