<?php

namespace App\Type;

abstract class MaterialType extends Enum {
    const Row_Material = "Row_Material";
    const Finished_Good = "Finished_Good";
}