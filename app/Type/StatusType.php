<?php

namespace App\Type;

abstract class StatusType extends Enum {
    const Active = 0;
    const Inactive = 1;
}