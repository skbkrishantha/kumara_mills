<?php

namespace App\Type;

abstract class UserRoleType extends Enum {
    const Admin = "U001";
    const Staff = "U002";
}