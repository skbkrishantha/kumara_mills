<?php

namespace App\Type;

abstract class AttachmentType extends Enum {
    const EmployeePhoto = "AT002";
    const ClientPhoto ="AT004";
    const DirectorPhoto = "AT005";
    const SupplierPhoto = "AT006";
    const ProductPhoto = "AT007";
}