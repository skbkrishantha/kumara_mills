<?php

namespace App\Type;

abstract class TransactionType extends Enum {
    const Credit = "Credit";
    const Debit = "Debit";
}