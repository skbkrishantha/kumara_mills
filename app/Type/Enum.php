<?php

namespace App\Type;

use Illuminate\Database\Eloquent\Collection;

use ReflectionClass;

abstract class Enum {   

    static function getKeys(){
        $class = new ReflectionClass(get_called_class());
        return array_keys($class->getConstants());
    }
    static function getKeyValue(){
        $class = new ReflectionClass(get_called_class());        
        return $class->getConstants();
    }
}
