<?php

namespace App\Type;

abstract class ModulePermissionType extends Enum {
    const create = "create";
    const view = "view";
    const update = "update";
    const delete = "delete";
    const visible="visible";
}