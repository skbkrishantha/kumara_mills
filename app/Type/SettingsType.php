<?php

namespace App\Type;

abstract class SettingsType extends Enum {
    const CurrencyPrefix="SET0003";
    const EPF_Employee_Contribution_Precentage="SET0006";
    const EPF_Company_Contribution_Precentage="SET0007";
    const ETF_Company_Contribution_Precentage="SET0008";
}