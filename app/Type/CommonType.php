<?php

namespace App\Type;

abstract class CommonType extends Enum {
    const LockoutTime = 300;
    const ImageWidth = 500;
    const ImageHight = 500;
    const CompanyId ="COM001";
}