<?php

namespace App\Type;

abstract class PaymentType extends Enum {
    const Cash = "Cash";
    const Cheque = "Cheque";
    const Loan = "Loan";
}