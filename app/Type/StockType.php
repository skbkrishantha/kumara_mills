<?php

namespace App\Type;

abstract class StockType extends Enum {
    const In = "In";
    const Out = "Out";
}