<?php

namespace App\Type;

abstract class ModuleType extends Enum {
    const Client = "Client";
    const Employee = "Employee";
    const Settings = "Settings";
    const Product = "Product";
    const Supplier = "Supplier";
    const Supply = "Supply";
    const Sell = "Sell";
    const Stock = "Stock";
    const CashBook = "CashBook";
    const Salary = "Salary";
}