<?php

namespace App\Util;
use DateTime;

use App\Model\Settings;
use App\Model\Company;

use App\Type\SettingsType;
use App\Type\CommonType;

class CommonFunc
{    
    public static function getPrimaryKey(){
        $digits = 3;
        $randPart=rand(1000, 9999);
        $time=time();
        return $time."".$randPart;
    }

    public static function getPaginationSize(){
        return 10;
    }

    public static function getCurrentDate(){
        $dt = new DateTime();
        return $dt->format('Y-m-d');
   }

    public static function getCurrencyFormat($currency){        
        return number_format(sprintf('%0.2f',$currency), 2, '.', ',');
    } 
    
    public static function getPrefixCurrencyFormat($currency){        
        $CurrencyPrefix=Settings::all()->where('id', '=',SettingsType::CurrencyPrefix)->first();
        return $CurrencyPrefix->value.''.self::getCurrencyFormat($currency);
    } 

    public static function getDateFormat($date){
        return date('Y-m-d',$date);
    }

    public static function getCompanyName(){
        $company = Company::find(CommonType::CompanyId);
        return strtoupper($company->name);
    }
}
