<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('uniqueModuleNameAndPermission', function ($attribute, $value, $parameters, $validator) {
            $count = DB::table('mst_module_permission')->where('module_name', $parameters[0])
                                        ->where('permission', $parameters[1])
                                        ->count();
            return $count === 0;
        },'Module Name and Permission must be unique');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
            return realpath(base_path().'/public');
        });
    }
}
