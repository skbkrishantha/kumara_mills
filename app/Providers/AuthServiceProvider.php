<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Policies\EmployeePolicy;
use App\Model\Employee;
use App\Policies\SettingsPolicy;
use App\Model\Settings;
use App\Policies\ClientPolicy;
use App\Policies\ProductPolicy;
use App\Policies\SupplierPolicy;
use App\Policies\SupplyPolicy;
use App\Policies\SellPolicy;
use App\Policies\StockPolicy;
use App\Policies\CashBookPolicy;
use App\Policies\SalaryPolicy;
use App\Model\Client;
use App\Model\Product;
use App\Model\Supplier;
use App\Model\Supply;
use App\Model\Sell;
use App\Model\Stock;
use App\Model\CashBook;
use App\Model\Salary;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Employee::class => EmployeePolicy::class,
        Settings::class => SettingsPolicy::class,
        Client::class => ClientPolicy::class,
        Product::class => ProductPolicy::class,
        Supplier::class => SupplierPolicy::class,
        Supply::class => SupplyPolicy::class,
        Sell::class => SellPolicy::class,
        Stock::class => StockPolicy::class,
        CashBook::class => CashBookPolicy::class,
        Salary::class => SalaryPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
