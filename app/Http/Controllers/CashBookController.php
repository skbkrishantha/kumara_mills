<?php

namespace App\Http\Controllers;

use App\Model\CashBook;
use App\Type\StatusType;
use App\Type\CommonType;
use App\Util\CommonFunc;
use Illuminate\Http\Request;

class CashBookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', CashBook::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $cashbooks = CashBook::select('tbl_cashbook.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_cashbook.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_cashbook.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('cashbook.list')->with('cashbooks',$cashbooks)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\CashBook  $cashBook
     * @return \Illuminate\Http\Response
     */
    public function show(CashBook $cashBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\CashBook  $cashBook
     * @return \Illuminate\Http\Response
     */
    public function edit(CashBook $cashBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\CashBook  $cashBook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CashBook $cashBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\CashBook  $cashBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(CashBook $cashBook)
    {
        //
    }
}
