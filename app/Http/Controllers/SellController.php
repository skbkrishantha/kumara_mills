<?php

namespace App\Http\Controllers;

use App\Model\Sell;
use App\Model\CashBook;
use App\Model\Stock;
use App\Model\Product;
use App\Model\Client;
use App\Model\Employee;
use App\Model\ProductUnit;
use Illuminate\Http\Request;

use App\Type\StatusType;
use App\Type\AttachmentType;
use App\Type\CommonType;
use App\Type\StockType;
use App\Type\TransactionType;
use App\Type\ModuleType;

use App\Util\CommonFunc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use PDF;

class SellController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Sell::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $sells = Sell::select('tbl_sell.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_sell.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_sell.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('sell.list')->with('sells',$sells)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Sell::class);

        if($request->input('skip')==null){
            StockController::removeSessionCart($request); 
        }

        $sell = new Sell(); 
        $cashbook = new CashBook();
        $product_list=Product::orderBy('display_name','asc')->pluck('display_name','id'); 
        $client_list=Client::orderBy('name','asc')->pluck('name','id'); 
        $employee_list=Employee::orderBy('name','asc')->pluck('name','id');   
        $product_unit_list=ProductUnit::orderBy('display_name','asc')->pluck('display_name','id');         

        return view('sell.create')->with('sell',$sell)
                                    ->with('cashbook',$cashbook)
                                    ->with('product_list',$product_list)
                                    ->with('client_list',$client_list)
                                    ->with('employee_list',$employee_list)
                                    ->with('product_unit_list',$product_unit_list);                               
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->authorize('create', Sell::class);      

        $amount=str_replace( ',', '', $request->input('amount'));
        $request->merge(['amount' => $amount]);
        $this->validate($request,[
            'date' => 'required',  
            'client_id' => 'required',      
            'employee_id' => 'required', 
            'amount' => 'required|numeric',           
        ]);
        
        $sell = new Sell;
        $sell->id=CommonFunc::getPrimaryKey();
        $sell->date = $request->input('date');
        $sell->client_id = $request->input('client_id');
        $sell->employee_id=$request->input('employee_id');
        $sell->comment=$request->input('comment');
        $sell->status=StatusType::Active;

        $cashbook = new CashBook;
        $cashbook->id=CommonFunc::getPrimaryKey();
        $cashbook->amount = $request->input('amount');
        $cashbook->payment_type = $request->input('payment_type');
        $cashbook->reference_no=$request->input('reference_no');
        $cashbook->effective_date=$request->input('effective_date');
        $cashbook->comment=$request->input('comment');
        $cashbook->transaction_type=TransactionType::Debit;
        $cashbook->status=StatusType::Active;

        DB::beginTransaction();
        try{
            StockController::$cart = StockController::getSessionCart($request);   
            
            $sell->save();

            foreach (StockController::$cart as $stock)
            {
                $stock->reference_id = $sell->id;                            
                $stock->module_type = ModuleType::Sell;
                $stock->save();
            }
            $cashbook->reference_id = $sell->id;
            $cashbook->module_type = ModuleType::Sell;
            $cashbook->save();

            DB::commit();
            $msg = "Transaction successfully completed!";
            $msgType= "success";
        } catch (Exception $e) {
            DB::rollBack();
            $msg = "Transaction failed!";
            $msgType= "error";
        }

        return redirect()->route('sell.index')
                         ->with($msgType,$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Sell::class);

        $sell =Sell::find($id);
        $cashbooks = $sell->cashbook;
        $stocks = $sell->stock;

        return view('sell.view')->with('sell',$sell)
                                  ->with('cashbooks',$cashbooks)
                                  ->with('carts',$stocks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $this->authorize('update', Sell::class);        
        if($request->input('skip')==null){
            StockController::removeSessionCart($request); 
        }

        $sell = Sell::find($id); 
        $cashbook = CashBook::where('reference_id','=', $sell->id)->firstOrFail();
        $product_list=Product::orderBy('display_name','asc')->pluck('display_name','id'); 
        $client_list=Client::orderBy('name','asc')->pluck('name','id'); 
        $employee_list=Employee::orderBy('name','asc')->pluck('name','id');   
        $product_unit_list=ProductUnit::orderBy('display_name','asc')->pluck('display_name','id'); 
        
        StockController::setSessionCart($request, $sell->stock);

        return view('sell.create')->with('sell',$sell)
                                    ->with('cashbook',$cashbook)
                                    ->with('product_list',$product_list)
                                    ->with('client_list',$client_list)
                                    ->with('employee_list',$employee_list)
                                    ->with('product_unit_list',$product_unit_list);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Sell::class);      

        $amount=str_replace( ',', '', $request->input('amount'));
        $request->merge(['amount' => $amount]);
        $this->validate($request,[
            'date' => 'required',  
            'client_id' => 'required',      
            'employee_id' => 'required', 
            'amount' => 'required|numeric',           
        ]);
        
        $sell = Sell::find($id);
        $sell->date = $request->input('date');
        $sell->client_id = $request->input('client_id');
        $sell->employee_id=$request->input('employee_id');
        $sell->comment=$request->input('comment');
        $sell->status=StatusType::Active;

        $cashbook = CashBook::where('reference_id','=', $sell->id)->firstOrFail();
        $cashbook->amount = $request->input('amount');
        $cashbook->payment_type = $request->input('payment_type');
        $cashbook->reference_no=$request->input('reference_no');
        $cashbook->effective_date=$request->input('effective_date');
        $cashbook->comment=$request->input('comment');
        $cashbook->transaction_type=TransactionType::Debit;
        $cashbook->status=StatusType::Active;
        
        DB::beginTransaction();
        try{
            StockController::$cart = StockController::getSessionCart($request);   

            $sell->save();

            $stock_exists = Stock::where('reference_id',$sell->id)->get();

            foreach ($stock_exists as $stockEx)
            {
                $flag = false;
                foreach (StockController::$cart  as $stock)
                {
                    if($stock->id == $stockEx->id){
                        $flag = true;
                    }   
                }
                if(!$flag){
                    $stockEx->delete();
                }
            }

            foreach (StockController::$cart  as $stock)
            {                
                $stock->reference_id = $sell->id;                      
                $stock->module_type = ModuleType::Sell;
                $stock->save();  
            }

            $cashbook->reference_id = $sell->id;            
            $cashbook->module_type = ModuleType::Sell;
            $cashbook->save();
            DB::commit();
            $msg = "Transaction successfully updated!";
            $msgType= "success";
        } catch (Exception $e) {
            DB::rollBack();
            $msg = "Transaction update failed!";
            $msgType= "error";
        }

        return redirect()->route('sell.index')
                         ->with($msgType,$msg);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sell $sell)
    {
        $this->authorize('delete', Sell::class);
    }

    public function print($id)
    {
        $this->authorize('view', Sell::class);
        
        $sell =Sell::find($id);
        $cashbooks = $sell->cashbook;
        $stocks = $sell->stock;

        $pdf = PDF::loadView('sell.print', ['sell'=>$sell,'cashbooks'=>$cashbooks,'carts'=>$stocks]);
        return $pdf->setPaper('a4')->setWarnings(false)->stream();
    }
}
