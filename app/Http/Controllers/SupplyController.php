<?php

namespace App\Http\Controllers;

use App\Model\Supply;
use App\Model\CashBook;
use App\Model\Stock;
use App\Model\Product;
use App\Model\Supplier;
use App\Model\Employee;
use App\Model\ProductUnit;
use Illuminate\Http\Request;

use App\Type\StatusType;
use App\Type\AttachmentType;
use App\Type\CommonType;
use App\Type\StockType;
use App\Type\TransactionType;
use App\Type\ModuleType;

use App\Util\CommonFunc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use PDF;

class SupplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Supply::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $supplys = Supply::select('tbl_supply.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_supply.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_supply.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('supply.list')->with('supplys',$supplys)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Supply::class);
        if($request->input('skip')==null){
            StockController::removeSessionCart($request); 
        }

        $supply = new Supply(); 
        $cashbook = new CashBook(); 
        $product_list=Product::orderBy('display_name','asc')->pluck('display_name','id'); 
        $supplier_list=Supplier::orderBy('name','asc')->pluck('name','id'); 
        $employee_list=Employee::orderBy('name','asc')->pluck('name','id');   
        $product_unit_list=ProductUnit::orderBy('display_name','asc')->pluck('display_name','id');     

        return view('supply.create')->with('supply',$supply)
                                    ->with('cashbook',$cashbook)
                                    ->with('product_list',$product_list)
                                    ->with('supplier_list',$supplier_list)
                                    ->with('employee_list',$employee_list)
                                    ->with('product_unit_list',$product_unit_list);                               
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->authorize('create', Supply::class);      

        $amount=str_replace( ',', '', $request->input('amount'));
        $request->merge(['amount' => $amount]);
        $this->validate($request,[
            'date' => 'required',  
            'supplier_id' => 'required',      
            'employee_id' => 'required',  
            'amount' => 'required|numeric',           
        ]);
        
        $supply = new Supply;
        $supply->id=CommonFunc::getPrimaryKey();
        $supply->date = $request->input('date');
        $supply->supplier_id = $request->input('supplier_id');
        $supply->employee_id=$request->input('employee_id');
        $supply->comment=$request->input('comment');
        $supply->status=StatusType::Active;

        $cashbook = new CashBook;
        $cashbook->id=CommonFunc::getPrimaryKey();
        $cashbook->amount = $request->input('amount');
        $cashbook->payment_type = $request->input('payment_type');
        $cashbook->reference_no=$request->input('reference_no');
        $cashbook->effective_date=$request->input('effective_date');
        $cashbook->comment=$request->input('comment');
        $cashbook->transaction_type=TransactionType::Credit;
        $cashbook->status=StatusType::Active;

        DB::beginTransaction();
        try{

            StockController::$cart = StockController::getSessionCart($request);   
            
            $supply->save();

            foreach (StockController::$cart as $stock)
            {
                $stock->reference_id = $supply->id;                      
                $stock->module_type = ModuleType::Supply;
                $stock->save();
            }
            $cashbook->reference_id = $supply->id;                  
            $cashbook->module_type = ModuleType::Supply;
            $cashbook->save();

            DB::commit();
            $msg = "Transaction successfully completed!";
            $msgType= "success";
        } catch (Exception $e) {
            DB::rollBack();
            $msg = "Transaction failed!";
            $msgType= "error";
        }

        return redirect()->route('supply.index')
                         ->with($msgType,$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Supply::class);

        $supply =Supply::find($id);
        $cashbooks = $supply->cashbook;
        $stocks = $supply->stock;

        return view('supply.view')->with('supply',$supply)
                                  ->with('cashbooks',$cashbooks)
                                  ->with('carts',$stocks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $this->authorize('update', Supply::class);
        

        if($request->input('skip')==null){
            StockController::removeSessionCart($request); 
        }

        $supply = Supply::find($id); 
        $cashbook = CashBook::where('reference_id','=', $supply->id)->firstOrFail();
        $product_list=Product::orderBy('display_name','asc')->pluck('display_name','id'); 
        $supplier_list=Supplier::orderBy('name','asc')->pluck('name','id'); 
        $employee_list=Employee::orderBy('name','asc')->pluck('name','id');   
        $product_unit_list=ProductUnit::orderBy('display_name','asc')->pluck('display_name','id'); 
        
        StockController::setSessionCart($request, $supply->stock);

        return view('supply.create')->with('supply',$supply)
                                    ->with('cashbook',$cashbook)
                                    ->with('product_list',$product_list)
                                    ->with('supplier_list',$supplier_list)
                                    ->with('employee_list',$employee_list)
                                    ->with('product_unit_list',$product_unit_list);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Supply::class); 
        
        $amount=str_replace( ',', '', $request->input('amount'));
        $request->merge(['amount' => $amount]);
        $this->validate($request,[
            'date' => 'required',  
            'supplier_id' => 'required',      
            'employee_id' => 'required', 
            'amount' => 'required|numeric',           
        ]);
        
        $supply = Supply::find($id);
        $supply->date = $request->input('date');
        $supply->supplier_id = $request->input('supplier_id');
        $supply->employee_id=$request->input('employee_id');
        $supply->comment=$request->input('comment');
        $supply->status=StatusType::Active;

        $cashbook = CashBook::where('reference_id','=', $supply->id)->firstOrFail();
        $cashbook->amount = $request->input('amount');
        $cashbook->payment_type = $request->input('payment_type');
        $cashbook->reference_no=$request->input('reference_no');
        $cashbook->effective_date=$request->input('effective_date');
        $cashbook->comment=$request->input('comment');
        $cashbook->transaction_type=TransactionType::Credit;
        $cashbook->status=StatusType::Active;
        
        DB::beginTransaction();
        try{
            StockController::$cart = StockController::getSessionCart($request);   

            $supply->save();

            $stock_exists = Stock::where('reference_id',$supply->id)->get();

            foreach ($stock_exists as $stockEx)
            {
                $flag = false;
                foreach (StockController::$cart  as $stock)
                {
                    if($stock->id == $stockEx->id){
                        $flag = true;
                    }   
                }
                if(!$flag){
                    $stockEx->delete();
                }
            }

            foreach (StockController::$cart  as $stock)
            {                
                $stock->reference_id = $supply->id;                
                $stock->module_type = ModuleType::Supply;
                $stock->save();  
            }

            $cashbook->reference_id = $supply->id;            
            $cashbook->module_type = ModuleType::Supply;
            $cashbook->save();
            DB::commit();
            $msg = "Transaction successfully updated!";
            $msgType= "success";
        } catch (Exception $e) {
            DB::rollBack();
            $msg = "Transaction update failed!";
            $msgType= "error";
        }

        return redirect()->route('supply.index')
                         ->with($msgType,$msg);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supply $supply)
    {
        $this->authorize('delete', Supply::class);
    }

    public function print($id)
    {
        $this->authorize('view', Supply::class);
        
        $supply =Supply::find($id);
        $cashbooks = $supply->cashbook;
        $stocks = $supply->stock;

        $pdf = PDF::loadView('supply.print', ['supply'=>$supply,'cashbooks'=>$cashbooks,'carts'=>$stocks]);
        return $pdf->setPaper('a4')->setWarnings(false)->stream();
    }
}
