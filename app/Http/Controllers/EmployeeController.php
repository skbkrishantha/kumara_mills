<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\UserAccount;
use App\Model\Employee;
use App\Model\Role;
use App\Model\EmployeeAttachment;
use App\Model\IdentityCardType;

use App\Type\StatusType;
use App\Type\AttachmentType;
use App\Type\CommonType;

use App\Util\CommonFunc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Employee::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }

        $role_list=Role::where('mst_user_role.STATUS',StatusType::Active)->orderBy('display_name','asc')->pluck('display_name', 'id');       
        
        $employees = Employee::select('tbl_employee.*')
                ->leftJoin('tbl_user_account', 'tbl_user_account.id', '=', 'tbl_employee.user_account_id')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_employee.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->when(!empty($request->input('search-role')) , function ($query) use($request){
                    return $query->where('tbl_user_account.ROLE_ID', '=',$request->input('search-role') );
                })
                ->where('tbl_employee.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('employee.list')->with('employees',$employees)
                                 ->with('role_list',$role_list)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Employee::class);

        $role_list=Role::orderBy('display_name','asc')->pluck('display_name', 'id');
        $identity_card_list=IdentityCardType::orderBy('display_name','asc')->pluck('display_name','id');

        $userAccount = new UserAccount();
        $employee = new Employee();

        return view('employee.create')->with('user_account',$userAccount)
                                   ->with('employee',$employee)
                                   ->with('role_list',$role_list)
                                   ->with('identity_card_list',$identity_card_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $this->authorize('create', Employee::class);
        
        $this->validate($request,[
            'name' => 'required',
            'identity_card_type_id' => 'required',
            'identity_card_number' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:tbl_employee',
            'mobile1' => 'required|regex:/(0)[0-9]{9}/|unique:tbl_employee',
            'address1'  => 'required',
            'user_name' => 'required',
            'email' => 'required|string|email|max:255|unique:tbl_user_account',
            'password' => 'required|string|min:6|confirmed',
            'role'  => 'required',
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg',              
        ]);

        $user_account_id=CommonFunc::getPrimaryKey();

        $user_account= new UserAccount;
        $user_account->id=$user_account_id;
        $user_account->email=$request->input('email');
        $user_account->lockout_time=CommonType::LockoutTime;
        $user_account->user_name=$request->input('user_name');
        $user_account->user_password=Hash::make($request->input('password'));
        $user_account->status=StatusType::Active;
        $user_account->role_id = $request -> input('role');

        $employee_id=CommonFunc::getPrimaryKey();

        $employee = new Employee;
        $employee->id=$employee_id;
        $employee->name = $request->input('name');
        $employee->employee_number = $request->input('employee_number');
        $employee->identity_card_type_id = $request->input('identity_card_type_id');
        $employee->identity_card_number = $request->input('identity_card_number');
        $employee->mobile1 = $request -> input('mobile1');
        $employee->address_01 = $request->input('address1');
        $employee->email=$request->input('email');
        $employee->joined_date=$request->input('joined_date');
        $employee->comment=$request->input('comment');
        $employee->status=StatusType::Active;
        $employee->user_account_id=$user_account_id;
               
        $user_account->save();
        $employee->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;

            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('storage/images/employee_attachments/' .$fileNameToStore01));

            if (!empty($employee->employee_photo()->id))
            {
                $employeeImage=$employee->employee_photo();
                Storage::delete('public/images/employee_attachments/'.$employeeImage->path);
                $employeeImage->path=$fileNameToStore01;
                $employeeImage->save();
            }else{
                $employeeImage=new EmployeeAttachment();
                $employeeImage->id=CommonFunc::getPrimaryKey();
                $employeeImage->path=$fileNameToStore01;
                $employeeImage->employee_id=$employee->id;
                $employeeImage->attachment_types_id=AttachmentType::EmployeePhoto;
                $employeeImage->status=StatusType::Active;
                $employeeImage->save();
            }
        }

        return redirect()->route('employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->id != $id)
        {
            $this->authorize('view', Employee::class);
        }

        $userAccount =UserAccount::find($id);
        if($userAccount->employee != null)
        {
            $employee = $userAccount->employee;
        }else{
            $employee=new Employee();
        }

        return view('employee.view')->with('user_account',$userAccount)
                                   ->with('employee',$employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->id != $id)
        {
            $this->authorize('update', Employee::class);
        }

        $role_list=Role::orderBy('display_name','asc')->pluck('display_name', 'id');        
        $identity_card_list=IdentityCardType::orderBy('display_name','asc')->pluck('display_name','id');

        $userAccount =UserAccount::find($id);
        if($userAccount->employee != null)
        {
            $employee = $userAccount->employee;
        }else{
            $employee=new Employee();
        }

        return view('employee.create')->with('user_account',$userAccount)
                                   ->with('employee',$employee)
                                   ->with('role_list',$role_list)
                                   ->with('identity_card_list',$identity_card_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->id != $id)
        {
            $this->authorize('update', Employee::class);
        }

        $user_account=UserAccount::find($id);
        
        if($user_account->employee != null)
        {
            $employee = $user_account->employee;
        }else{
            $employee=new Employee();
            $employee->id=CommonFunc::getPrimaryKey();
        }
        
        $this->validate($request,[
            'name' => 'required',            
            'identity_card_type_id' => 'required',
            'identity_card_number' => 'required|regex:/[0-9]{9,16}/|unique:tbl_employee,identity_card_number,'.$employee->id,
            'mobile1' => 'required|regex:/(0)[0-9]{9}/|unique:tbl_employee,mobile1,'.$employee->id,
            'address1'  => 'required',
            'user_name' => 'required',
            'email' => 'required|string|email|max:255|unique:tbl_employee,email,'.$employee->id,
            'password' => 'nullable|min:6|confirmed',
            'role'  => 'sometimes', 
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg',             
        ]);       

        $user_account->lockout_time=CommonType::LockoutTime;
        $user_account->email=$request->input('email');
        $user_account->user_name=$request->input('user_name');
        if(!empty($request->input('password')))
        {      
            $user_account->user_password=Hash::make($request->input('password'));
        }
        $user_account->status=StatusType::Active;
        $user_account->role_id = $request -> input('role');

        $employee->name = $request->input('name');
        $employee->employee_number = $request->input('employee_number');
        $employee->identity_card_type_id = $request->input('identity_card_type_id');
        $employee->identity_card_number = $request->input('identity_card_number');
        $employee->mobile1 = $request -> input('mobile1');
        $employee->address_01 = $request->input('address1');
        $employee->email=$request->input('email');
        $employee->joined_date=$request->input('joined_date');
        $employee->comment=$request->input('comment');
        $employee->status=StatusType::Active;
        $employee->user_account_id=$user_account->id;
               
        $user_account->save();
        $employee->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;

            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('/storage/images/employee_attachments/' .$fileNameToStore01));

            if (!empty($employee->employee_photo()->id))
            {
                $employeeImage=$employee->employee_photo();
                Storage::delete('public/images/employee_attachments/'.$employeeImage->path);
                $employeeImage->path=$fileNameToStore01;
                $employeeImage->save();
            }else{
                $employeeImage=new EmployeeAttachment();
                $employeeImage->id=CommonFunc::getPrimaryKey();
                $employeeImage->path=$fileNameToStore01;
                $employeeImage->employee_id=$employee->id;
                $employeeImage->attachment_types_id='AT002';
                $employeeImage->status=StatusType::Active;
                $employeeImage->save();
            }
        }
        if(Auth::user()->id == $id)
        {
            if(!empty($request->input('password')))
            {
                Auth::logout();
                return redirect()->route('login');
            }
            return redirect()->route('employee.show',$id);
        }                    
        return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Employee::class);
    }
}
