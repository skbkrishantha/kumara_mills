<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Event;
use App\Type\StatusType;
use App\Util\CommonFunc;

use Illuminate\Support\Facades\Auth;

use Calendar;

use DateTime;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $events = [];
        $data = Event::all()->where('user_account_id', '=',Auth::user()->id)
                            ->where('status', '=',StatusType::Active);

        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new DateTime($value->start_date),
                    new DateTime($value->end_date.' +1 day'),                    
                    $value->id,
                    // Add color and link on event
                    [
                        'color' => '#777',
                        //'url' => 'pass here url and any route',
                    ]
                );
            }
        }

        $calendar = Calendar::addEvents($events)
            ->setCallbacks([
                'themeSystem' => '"bootstrap4"',
                'eventRender' => 'function(event, element) {
                    element.html(event.title + "<span class=pull-right id=Delete> X </span>");
                    element.attr("id",event.id);                    
                 }',
                 'eventClick' => 'function(calEvent, jsEvent, view) {
                    if (jsEvent.target.id === "Delete") {
                      var id=calEvent.id;
                      $.ajax({
                        url:"event/delete_event/"+id,
                        type: "get",
                        success:function(msg)
                        {                    
                           $("#calendar-events").fullCalendar("removeEvents",id);
                           alertify.success("Event Deleted");                                             
                        },
                        error: function (error) {
                            alertify.error("Error Occured"); 
                        }
                      });
                    }
                  }'
            ]);        
        $calendar->setId('events'); 

        return view('event.list', compact('calendar'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate($request,[
            'title'  => 'required',
            'from' => 'required',
            'to'  => 'required',              
        ]);*/
        
        $event = new Event;
        $event->id=CommonFunc::getPrimaryKey();
        $event->title = $request->input('title');
        $event->start_date = $request->input('from');
        $event->end_date = $request -> input('to');
        $event->status=StatusType::Active;
        $event->user_account_id=Auth::user()->id;
               
        $event->save();

        return redirect()->route('event.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id;
    }


    //ajax
    public function delete($id)
    {
        return Event::where('id',$id)->delete();
    }
    
    public function calander_view()
    {
        $events = [];
        $data = Event::all()->where('user_account_id', '=',Auth::user()->id)
                            ->where('status', '=',StatusType::Active);

        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new DateTime($value->start_date),
                    new DateTime($value->end_date.' +1 day'),                    
                    $value->id,
                    // Add color and link on event
                    [
                        'color' => '#777',
                        //'url' => 'pass here url and any route',
                    ]
                );
            }
        }

        $calendar = Calendar::addEvents($events);
        return view('event.calander', compact('calendar'));
    }
}
