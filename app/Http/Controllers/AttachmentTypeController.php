<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\AttachmentType;

use App\Model\Settings;
use App\Util\CommonFunc;
use App\Type\StatusType;

class AttachmentTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Settings::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $attachment_types = AttachmentType::select('mst_attachment_types.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('mst_attachment_types.name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('mst_attachment_types.status', '=',$request->input('search-status') )
                ->orderBy('insert_datetime', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('attachment_type.list')->with('attachment_types',$attachment_types)
                                              ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Settings::class);

        $attachment_type = new AttachmentType();

        return view('attachment_type.create')->with('attachment_type',$attachment_type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_attachment_types',
            'display_name' => 'required|unique:mst_attachment_types',              
        ]);

        $attachment_type= new AttachmentType;
        $attachment_type->id=CommonFunc::getPrimaryKey();
        $attachment_type->name=$request->input('name');
        $attachment_type->display_name=$request->input('display_name');
        $attachment_type->status=StatusType::Active;
               
        $attachment_type->save();

        return redirect()->route('attachment_type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttachmentType  $attachmentType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Settings::class);

        $attachment_type = AttachmentType::find($id);

        return view('attachment_type.view')->with('attachment_type',$attachment_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttachmentType  $attachmentType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Settings::class);

        $attachment_type = AttachmentType::find($id);

        return view('attachment_type.create')->with('attachment_type',$attachment_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttachmentType  $attachmentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_attachment_types,name,'.$id,
            'display_name' => 'required|unique:mst_attachment_types,display_name,'.$id,              
        ]);

        $attachment_type= AttachmentType::find($id);
        $attachment_type->name=$request->input('name');
        $attachment_type->display_name=$request->input('display_name');
               
        $attachment_type->save();

        return redirect()->route('attachment_type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttachmentType  $attachmentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttachmentType $attachmentType)
    {
        //
    }
}
