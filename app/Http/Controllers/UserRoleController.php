<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ModulePermission;

use App\Model\Role;
use App\Model\Settings;
use App\Util\CommonFunc;
use App\Type\StatusType;

class UserRoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Settings::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $user_roles = Role::select('mst_user_role.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('mst_user_role.name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('mst_user_role.status', '=',$request->input('search-status') )
                ->orderBy('insert_datetime', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('user_role.list')->with('user_roles',$user_roles)
                                     ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Settings::class);

        $user_role = new Role();
        $module_permissions= ModulePermission::all()->sortBy('module_name')->sortBy('permission');
        $modules=$module_permissions->unique('module_name');

        return view('user_role.create')->with('user_role',$user_role)
                                        ->with('module_permissions',$module_permissions)
                                        ->with('modules',$modules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_user_role',
            'display_name' => 'required|unique:mst_user_role',              
        ]);

        $user_role= new Role;
        $user_role->id=CommonFunc::getPrimaryKey();
        $user_role->name=$request->input('name');
        $user_role->display_name=$request->input('display_name');
        $user_role->status=StatusType::Active;
               
        $user_role->save();
        $module_permission_ids = $request->input('module_permission') ;
        $user_role->permissions()->sync($module_permission_ids);

        return redirect()->route('user_role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Settings::class);

        $user_role = role::find($id);

        return view('user_role.view')->with('user_role',$user_role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Settings::class);
        $module_permissions= ModulePermission::all()->sortBy('module_name')->sortBy('permission');
        $modules=$module_permissions->unique('module_name');

        $user_role = role::find($id);
        
        return view('user_role.create')->with('user_role',$user_role)
                                       ->with('module_permissions',$module_permissions)
                                       ->with('modules',$modules);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_user_role,name,'.$id,
            'display_name' => 'required|unique:mst_user_role,display_name,'.$id,              
        ]);

        $user_role= role::find($id);
        $user_role->name=$request->input('name');
        $user_role->display_name=$request->input('display_name');
        $user_role->status=StatusType::Active;
               
        $user_role->save();    
        $module_permission_ids = $request->input('module_permission') ;
        $user_role->permissions()->sync($module_permission_ids);

        return redirect()->route('user_role.index');
    }

    /** 
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete', Settings::class);
    }    
}
