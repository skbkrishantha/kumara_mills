<?php

namespace App\Http\Controllers;

use App\EmployeeAttachment;
use Illuminate\Http\Request;

class EmployeeAttachmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAttachment $employeeAttachment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAttachment $employeeAttachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAttachment $employeeAttachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeAttachment  $employeeAttachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAttachment $employeeAttachment)
    {
        //
    }
}
