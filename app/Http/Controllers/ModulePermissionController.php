<?php

namespace App\Http\Controllers;

use App\Model\ModulePermission;
use Illuminate\Http\Request;

use App\Model\Role;
use App\Model\Settings;
use App\Util\CommonFunc;
use App\Type\StatusType;

class ModulePermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Settings::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $module_permissions = ModulePermission::select('mst_module_permission.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('mst_module_permission.module_name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('mst_module_permission.status', '=',$request->input('search-status') )
                ->orderBy('mst_module_permission.module_name', 'ASC')  
                ->orderBy('mst_module_permission.permission', 'ASC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('module_permission.list')->with('module_permissions',$module_permissions)
                                     ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Settings::class);

        $module_permission = new ModulePermission;
        
        return view('module_permission.create')->with('module_permission',$module_permission);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'module_name' => "required|uniqueModuleNameAndPermission:$request->module_name,$request->permission",
            'permission' => "required|uniqueModuleNameAndPermission:$request->module_name,$request->permission",               
        ]);

        $module_permission= new ModulePermission;
        $module_permission->id=CommonFunc::getPrimaryKey();
        $module_permission->module_name=$request->input('module_name');
        $module_permission->permission=$request->input('permission');
        $module_permission->status=StatusType::Active;
               
        $module_permission->save();

        return redirect()->route('module_permission.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModulePermission  $modulePermission
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Settings::class);

        $module_permission = ModulePermission::find($id);

        return view('module_permission.view')->with('module_permission',$module_permission);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModulePermission  $modulePermission
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Settings::class);

        $module_permission = ModulePermission::find($id);
        
        return view('module_permission.create')->with('module_permission',$module_permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModulePermission  $modulePermission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModulePermission $modulePermission)
    {
        $this->authorize('update', Settings::class);

        $this->validate($request,[
            'module_name' => "required|uniqueModuleNameAndPermission:$request->module_name,$request->permission",
            'permission' => "required|uniqueModuleNameAndPermission:$request->module_name,$request->permission",               
        ]);

        $module_permission= $modulePermission;
        $module_permission->module_name=$request->input('module_name');
        $module_permission->permission=$request->input('permission');
        $module_permission->status=StatusType::Active;
               
        $module_permission->save();

        return redirect()->route('module_permission.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModulePermission  $modulePermission
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModulePermission $modulePermission)
    {
        $this->authorize('delete', Settings::class);
    }
}
