<?php

namespace App\Http\Controllers;

use App\Model\ProductUnit;
use App\Model\Settings;
use Illuminate\Http\Request;
use App\Util\CommonFunc;
use App\Type\StatusType;

class ProductUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Settings::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $product_units = ProductUnit::select('mst_product_units.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('mst_product_units.name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('mst_product_units.status', '=',$request->input('search-status') )
                ->orderBy('insert_datetime', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('product_unit.list')->with('product_units',$product_units)
                                     ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Settings::class);

        $product_unit = new ProductUnit();

        return view('product_unit.create')->with('product_unit',$product_unit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_product_units',
            'display_name' => 'required|unique:mst_product_units',              
        ]);

        $product_unit= new ProductUnit;
        $product_unit->id=CommonFunc::getPrimaryKey();
        $product_unit->name=$request->input('name');
        $product_unit->display_name=$request->input('display_name');
        $product_unit->weight=$request->input('weight');
        $product_unit->status=StatusType::Active;
               
        $product_unit->save();

        return redirect()->route('product_unit.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductUnit  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function show(ProductUnit $productUnit)
    {
        $this->authorize('view', Settings::class);

        return view('product_unit.view')->with('product_unit',$productUnit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductUnit  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductUnit $productUnit)
    {
        $this->authorize('view', Settings::class);

        return view('product_unit.create')->with('product_unit',$productUnit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductUnit  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductUnit $product_unit)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_product_units,name,'.$product_unit->id,
            'display_name' => 'required|unique:mst_product_units,display_name,'.$product_unit->id,              
        ]);

        $product_unit->name=$request->input('name');
        $product_unit->display_name=$request->input('display_name');
        $product_unit->weight=$request->input('weight');
        $product_unit->status=StatusType::Active;
        $product_unit->save();

        return redirect()->route('product_unit.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductUnit  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductUnit $productUnit)
    {
        //
    }
}
