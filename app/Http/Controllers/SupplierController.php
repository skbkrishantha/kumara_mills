<?php

namespace App\Http\Controllers;

use App\Model\Supplier;
use App\Model\SupplierAttachment;
use Illuminate\Http\Request;
use App\Model\IdentityCardType;

use App\Type\StatusType;
use App\Type\AttachmentType;
use App\Type\CommonType;

use App\Util\CommonFunc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Intervention\Image\ImageManagerStatic as Image;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Supplier::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $suppliers = Supplier::select('tbl_supplier.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_supplier.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_supplier.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('supplier.list')->with('suppliers',$suppliers)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Supplier::class);

        $supplier = new Supplier();        
        $identity_card_list=IdentityCardType::orderBy('display_name','asc')->pluck('display_name','id');

        return view('supplier.create')->with('supplier',$supplier)
                                    ->with('identity_card_list',$identity_card_list);                                 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->authorize('create', Supplier::class);

        $this->validate($request,[
            'name' => 'required',
            'identity_card_type_id' => 'required',
            'identity_card_number' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:tbl_supplier',
            'mobile1' => 'required|regex:/(0)[0-9]{9}/|unique:tbl_supplier',
            'address1'  => 'required',
            'email' => 'nullable|string|email|max:255|unique:tbl_supplier',
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg',              
        ]);
        
        $supplier = new Supplier;
        $supplier->id=CommonFunc::getPrimaryKey();
        $supplier->name = $request->input('name');
        $supplier->identity_card_type_id = $request->input('identity_card_type_id');
        $supplier->identity_card_number = $request->input('identity_card_number');
        $supplier->mobile1 = $request -> input('mobile1');
        $supplier->address1 = $request->input('address1');
        $supplier->email=$request->input('email');
        $supplier->joined_date=$request->input('joined_date');
        $supplier->comment=$request->input('comment');
        $supplier->status=StatusType::Active;
               
        $supplier->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getSupplierOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getSupplierOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;

            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('/storage/images/supplier_attachments/' .$fileNameToStore01));

            if (!empty($supplier->supplier_photo()->id))
            {
                $supplierImage=$supplier->supplier_photo();
                Storage::delete('public/images/supplier_attachments/'.$supplierImage->path);
                $supplierImage->path=$fileNameToStore01;
                $supplierImage->save();
            }else{
                $supplierImage=new SupplierAttachment();
                $supplierImage->id=CommonFunc::getPrimaryKey();
                $supplierImage->path=$fileNameToStore01;
                $supplierImage->supplier_id=$supplier->id;
                $supplierImage->attachment_types_id=AttachmentType::SupplierPhoto;
                $supplierImage->status=StatusType::Active;
                $supplierImage->save();
            }
        }

        return redirect()->route('supplier.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Supplier::class);

        $supplier =Supplier::find($id);
        
        return view('supplier.view')->with('supplier',$supplier);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Supplier::class);

        $supplier = Supplier::find($id);
        $identity_card_list=IdentityCardType::orderBy('display_name','asc')->pluck('display_name','id');

        return view('supplier.create')->with('supplier',$supplier)
                                    ->with('identity_card_list',$identity_card_list); ; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Supplier::class);

        $this->validate($request,[
            'name' => 'required',
            'identity_card_type_id' => 'required',
            'identity_card_number' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:tbl_supplier,identity_card_number,'.$id,
            'mobile1' => 'required|regex:/(0)[0-9]{9}/|unique:tbl_supplier,mobile1,'.$id,
            'address1'  => 'required',
            'email' => 'nullable|string|email|max:255|unique:tbl_supplier,email,'.$id,
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg',              
        ]);
        
        $supplier = Supplier::find($id);
        $supplier->name = $request->input('name');
        $supplier->identity_card_type_id = $request->input('identity_card_type_id');
        $supplier->identity_card_number = $request->input('identity_card_number');
        $supplier->mobile1 = $request -> input('mobile1');
        $supplier->address1 = $request->input('address1');
        $supplier->email=$request->input('email');
        $supplier->joined_date=$request->input('joined_date');
        $supplier->comment=$request->input('comment');
        $supplier->status=StatusType::Active;
               
        $supplier->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalName();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;
            
            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('/storage/images/supplier_attachments/' .$fileNameToStore01));

            if (!empty($supplier->supplier_photo()->id))
            {
                $supplierImage=$supplier->supplier_photo();
                Storage::delete('public/images/supplier_attachments/'.$supplierImage->path);
                $supplierImage->path=$fileNameToStore01;
                $supplierImage->save();
            }else{
                $supplierImage=new SupplierAttachment();
                $supplierImage->id=CommonFunc::getPrimaryKey();
                $supplierImage->path=$fileNameToStore01;
                $supplierImage->supplier_id=$supplier->id;
                $supplierImage->attachment_types_id=AttachmentType::SupplierPhoto;
                $supplierImage->status=StatusType::Active;
                $supplierImage->save();
            }
        }

        return redirect()->route('supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $this->authorize('delete', Supplier::class);
    }
}
