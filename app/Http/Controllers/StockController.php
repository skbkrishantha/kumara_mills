<?php

namespace App\Http\Controllers;

use App\Model\Stock;
use App\Model\Product;
use App\Type\StatusType;
use App\Type\StockType;
use App\Type\CommonType;
use App\Util\CommonFunc;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Exception;
use DB;
use Response;

class StockController extends Controller
{

    public static $cart_session_key = 'cart';
    public static $cart;

    public function __construct()
    {
        StockController::$cart = new Collection(new Stock);
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Stock::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $stocks = Stock::select('tbl_stock.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_stock.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_stock.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('stock.list')->with('stocks',$stocks)
                                 ->with('request',$request);
    }

    public function dashboard(Request $request)
    {
        $this->authorize('view', Stock::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $stocks = Stock::select(DB::raw('product_id,SUM(CASE WHEN tbl_stock.stock_type = \'In\' THEN tbl_stock.units ELSE 0  END) - SUM(CASE WHEN tbl_stock.stock_type = \'Out\' THEN tbl_stock.units ELSE 0  END) as units'))
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_stock.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_stock.STATUS', '=',$request->input('search-status') )
                ->groupBy('product_id')->get();

        return view('stock.dashboard')->with('stocks',$stocks)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }

    public function getStockByProductId(Request $request,$id)
    {
        $stock_units = (double) $this->getStockUnits($request,$id);
            
        return Stock::where('product_id', $id)
            ->join('tbl_product', 'tbl_product.id', '=', 'tbl_stock.product_id')
            ->where('stock_type', StockType::In)
            ->groupBy('product_id','product_unit_id')
            ->selectraw('product_id,'.$stock_units.' as units,tbl_stock.product_unit_id,tbl_product.current_price as unit_price')
            ->get()->first();
    }

    public function stockExists(Request $request)
    {
        $units = (double) $request->get('units');
        $product_id = $request->get('product_id');            
        $stock_units = $this->getStockUnits($request,$product_id);
        return $units <= $stock_units ? 'true':'false';
    }

    public function getStockUnits(Request $request,$product_id){
        
        $stock_units_in = 0;
        $stock_units_out = 0;
        $stock_units_out_session  = 0;
        $stock_units_in_session  = 0;

        try{
            $stock_units_in = (double)  Stock::where('product_id', $product_id)
                ->where('stock_type', StockType::In)
                ->sum('units');

            $stock_units_out = (double)  Stock::where('product_id', $product_id)
                ->where('stock_type', StockType::Out)
                ->sum('units');

            $stockExList = Stock::where('product_id', $product_id)->get();            
            $stockList = StockController::getSessionCart($request)->where('product_id','===', $product_id);

                foreach ($stockList as $stock) {
                    $flag = false;
                    foreach ($stockExList as $stockEx) {
                        if($stockEx->id == $stock->id){
                            $flag = true;
                        }
                    }
                    if(!$flag){
                        if($stock->stock_type == StockType::In){
                            $stock_units_in_session += $stock->units;
                        }else{
                            $stock_units_out_session += $stock->units;
                        }                         
                    }
                }

        }catch(Exception $e){
            $stock_units_in = 0;
            $stock_units_out = 0;
            $stock_units_out_session  = 0;
            $stock_units_in_session  = 0;
        }
        return  (double)  ($stock_units_in + $stock_units_in_session) - ($stock_units_out + $stock_units_out_session);
    }

    public function outAddToCart(Request $request)
    {
        $stock = new Stock;
        $stock->id=CommonFunc::getPrimaryKey();
        $stock->product_id = $request->input('product_id');
        $stock->units = $request->input('units');
        $stock->product_unit_id = $request -> input('product_unit_id');        
        $stock->unit_price = $request -> input('unit_price');
        $stock->stock_type=StockType::Out;
        $stock->status=StatusType::Active;;

        StockController::$cart = StockController::getSessionCart($request);  
        StockController::$cart->push($stock);

        $request->session()->put(StockController::$cart_session_key,StockController::$cart);

        return view('stock.cart_view')->with('carts',$request->session()->get(StockController::$cart_session_key));

    }

    public function inAddToCart(Request $request)
    {
        $stock = new Stock;
        $stock->id=CommonFunc::getPrimaryKey();
        $stock->product_id = $request->input('product_id');
        $stock->units = $request->input('units');
        $stock->product_unit_id = $request -> input('product_unit_id');
        $stock->unit_price = $request -> input('unit_price');
        $stock->stock_type = StockType::In;
        $stock->status=StatusType::Active;

        StockController::$cart = StockController::getSessionCart($request);  
        StockController::$cart->push($stock);

        $request->session()->put(StockController::$cart_session_key,StockController::$cart);

        return view('stock.cart_view')->with('carts',$request->session()->get(StockController::$cart_session_key));

    }

    public function removeFromCart(Request $request,$id)
    {
        StockController::$cart = StockController::getSessionCart($request); 

        foreach (StockController::$cart as $key => $value) {
            if ($value->id == $id) {
                StockController::$cart->forget($key);
            }
        }

        $request->session()->put(StockController::$cart_session_key,StockController::$cart);

        return view('stock.cart_view')->with('carts',$request->session()->get(StockController::$cart_session_key));

    }

    public static function setSessionCart(Request $request, Collection $cart){
        StockController::$cart = new Collection(new Stock);
        foreach ($cart as $stock)
        {
            StockController::$cart->push($stock);
        }
        $request->session()->put(StockController::$cart_session_key,StockController::$cart);
    }

    public static function getSessionCart(Request $request){
        if($request->session()->get(StockController::$cart_session_key) == null){ 
            StockController::$cart = new Collection(new Stock);          
            $request->session()->put(StockController::$cart_session_key,StockController::$cart);
        }

        return $request->session()->get(StockController::$cart_session_key);
    }

    public static function removeSessionCart(Request $request){
        $request->session()->forget(StockController::$cart_session_key);
    }

    public function sellGraphData()
    {
        /*$stocks = Stock::select(DB::raw('sum(units) as units'),DB::raw('DATE(tbl_stock.update_datetime) as date'),'tbl_product.display_name as product_name')
                ->join('tbl_product', 'tbl_product.id', '=', 'tbl_stock.product_id')
                ->where('stock_type', StockType::Out)
                ->groupBy('product_id',DB::raw('DATE(tbl_stock.update_datetime)'))
                ->get();*/

        $stocks = Stock::select(DB::raw('SUM(CASE WHEN tbl_stock.stock_type = \'In\' THEN (tbl_stock.units * mst_product_units.weight) ELSE 0  END) - SUM(CASE WHEN tbl_stock.stock_type = \'Out\' THEN (tbl_stock.units * mst_product_units.weight) ELSE 0  END) as units'),'tbl_product.display_name as product_name')
                ->join('tbl_product', 'tbl_product.id', '=', 'tbl_stock.product_id')
                ->join('mst_product_units', 'mst_product_units.id', '=', 'tbl_stock.product_unit_id')
                ->groupBy('product_id')
                ->get();
        return array('data'=>$stocks,'xkey'=>'product_name','ykeys'=>array('units'),'labels'=>$stocks->pluck('product_name'));

    }

}
