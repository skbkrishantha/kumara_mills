<?php

namespace App\Http\Controllers;

use App\Model\BaseModel;
use App\Model\Client;
use App\Model\ClientAttachment;
use App\Model\IdentityCardType;

use App\Type\StatusType;
use App\Type\AttachmentType;
use App\Type\CommonType;

use App\Util\CommonFunc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Intervention\Image\ImageManagerStatic as Image;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Client::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $clients = Client::select('tbl_client.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_client.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_client.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('client.list')->with('clients',$clients)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Client::class);

        $client = new Client();        
        $identity_card_list=IdentityCardType::orderBy('display_name','asc')->orderBy('display_name','asc')->pluck('display_name','id');

        return view('client.create')->with('client',$client)
                                    ->with('identity_card_list',$identity_card_list);                                 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->authorize('create', Client::class);

        $this->validate($request,[
            'name' => 'required',
            'identity_card_type_id' => 'required',
            'identity_card_number' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:tbl_client',
            'mobile1' => 'required|regex:/(0)[0-9]{9}/|unique:tbl_client',
            'address1'  => 'required',
            'email' => 'nullable|string|email|max:255|unique:tbl_client',
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg',              
        ]);
        
        $client = new Client;
        $client->id=CommonFunc::getPrimaryKey();
        $client->name = $request->input('name');
        $client->identity_card_type_id = $request->input('identity_card_type_id');
        $client->identity_card_number = $request->input('identity_card_number');
        $client->mobile1 = $request -> input('mobile1');
        $client->address1 = $request->input('address1');
        $client->email=$request->input('email');
        $client->joined_date=$request->input('joined_date');
        $client->comment=$request->input('comment');
        $client->status=StatusType::Active;
               
        $client->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;

            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('/storage/images/client_attachments/' .$fileNameToStore01));

            if (!empty($client->client_photo()->id))
            {
                $clientImage=$client->client_photo();
                Storage::delete('public/images/client_attachments/'.$clientImage->path);
                $clientImage->path=$fileNameToStore01;
                $clientImage->save();
            }else{
                $clientImage=new ClientAttachment();
                $clientImage->id=CommonFunc::getPrimaryKey();
                $clientImage->path=$fileNameToStore01;
                $clientImage->client_id=$client->id;
                $clientImage->attachment_types_id=AttachmentType::ClientPhoto;
                $clientImage->status=StatusType::Active;
                $clientImage->save();
            }
        }

        return redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Client::class);

        $client =Client::find($id);
        
        return view('client.view')->with('client',$client);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Client::class);

        $client = Client::find($id);
        $identity_card_list=IdentityCardType::orderBy('display_name','asc')->pluck('display_name','id');

        return view('client.create')->with('client',$client)
                                    ->with('identity_card_list',$identity_card_list); ; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Client::class);

        $this->validate($request,[
            'name' => 'required',
            'identity_card_type_id' => 'required',
            'identity_card_number' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:tbl_client,identity_card_number,'.$id,
            'mobile1' => 'required|regex:/(0)[0-9]{9}/|unique:tbl_client,mobile1,'.$id,
            'address1'  => 'required',
            'email' => 'nullable|string|email|max:255|unique:tbl_client,email,'.$id,
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg',              
        ]);
        
        $client = Client::find($id);
        $client->name = $request->input('name');
        $client->identity_card_type_id = $request->input('identity_card_type_id');
        $client->identity_card_number = $request->input('identity_card_number');
        $client->mobile1 = $request -> input('mobile1');
        $client->address1 = $request->input('address1');
        $client->email=$request->input('email');
        $client->joined_date=$request->input('joined_date');
        $client->comment=$request->input('comment');
        $client->status=StatusType::Active;
               
        $client->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;
            
            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('/storage/images/client_attachments/' .$fileNameToStore01));

            if (!empty($client->client_photo()->id))
            {
                $clientImage=$client->client_photo();
                Storage::delete('public/images/client_attachments/'.$clientImage->path);
                $clientImage->path=$fileNameToStore01;
                $clientImage->save();
            }else{
                $clientImage=new ClientAttachment();
                $clientImage->id=CommonFunc::getPrimaryKey();
                $clientImage->path=$fileNameToStore01;
                $clientImage->client_id=$client->id;
                $clientImage->attachment_types_id=AttachmentType::ClientPhoto;
                $clientImage->status=StatusType::Active;
                $clientImage->save();
            }
        }

        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $this->authorize('delete', Client::class);
    }
}
