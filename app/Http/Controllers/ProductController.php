<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\ProductUnit;
use Illuminate\Http\Request;
use App\Util\CommonFunc;
use App\Type\StatusType;
use App\Type\CommonType;

use App\Model\ProductAttachment;
use App\Type\AttachmentType;

use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Product::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $products = Product::select('tbl_product.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_product.name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_product.status', '=',$request->input('search-status') )
                ->orderBy('insert_datetime', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('product.list')->with('products',$products)
                                     ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Product::class);

        $product = new Product();
        $product_unit_list=ProductUnit::orderBy('display_name','asc')->pluck('display_name','id');

        return view('product.create')->with('product',$product)
                                    ->with('product_unit_list',$product_unit_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Product::class);

        $current_price=str_replace( ',', '', $request->input('current_price'));
        $request->merge(['current_price' => $current_price]);

        $this->validate($request,[
            'name' => 'required|unique:tbl_product',
            'display_name' => 'required|unique:tbl_product',
            'image-01' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'current_price' => 'required|numeric',              
        ]);

        $product= new Product;
        $product->id=CommonFunc::getPrimaryKey();
        $product->name=$request->input('name');
        $product->display_name=$request->input('display_name');
        $product->current_price=$request->input('current_price');
        $product->product_unit_id=$request->input('product_unit_id');        
        $product->material_type=$request->input('material_type');
        $product->status=StatusType::Active;
               
        $product->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;

            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('storage/images/product_attachments/' .$fileNameToStore01));

            if (!empty($product->product_photo()->id))
            {
                $productImage=$product->product_photo();
                Storage::delete('public/images/product_attachments/'.$productImage->path);
                $productImage->path=$fileNameToStore01;
                $productImage->save();
            }else{
                $productImage=new ProductAttachment();
                $productImage->id=CommonFunc::getPrimaryKey();
                $productImage->path=$fileNameToStore01;
                $productImage->product_id=$product->id;
                $productImage->attachment_types_id=AttachmentType::ProductPhoto;
                $productImage->status=StatusType::Active;
                $productImage->save();
            }
        }

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Product::class);
        $product = Product::find($id);
        return view('product.view')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('view', Product::class);

        $product_unit_list=ProductUnit::orderBy('display_name','asc')->pluck('display_name','id');
        return view('product.create')->with('product',$product)
                                     ->with('product_unit_list',$product_unit_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('create', Product::class);

        $current_price=str_replace( ',', '', $request->input('current_price'));
        $request->merge(['current_price' => $current_price]);

        $this->validate($request,[
            'name' => 'required|unique:tbl_product,name,'.$product->id,
            'display_name' => 'required|unique:tbl_product,display_name,'.$product->id,                
            'current_price' => 'required|numeric',           
        ]);
 
        $product->name=$request->input('name');
        $product->display_name=$request->input('display_name');
        $product->product_unit_id=$request->input('product_unit_id');        
        $product->current_price=$request->input('current_price');        
        $product->material_type=$request->input('material_type');
        $product->status=StatusType::Active;
        $product->save();

        if($request->hasFile('image-01')){
            $fileNameWithExt = $request -> file('image-01')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extention = $request ->file('image-01')->getClientOriginalExtension();
            $fileNameToStore01 = $filename.'_'.CommonFunc::getPrimaryKey().'.'.$extention;

            $image= $request->file('image-01');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(CommonType::ImageWidth, CommonType::ImageHight);
            $image_resize->save(public_path('storage/images/product_attachments/' .$fileNameToStore01));

            if (!empty($product->product_photo()->id))
            {
                $productImage=$product->product_photo();
                Storage::delete('public/images/product_attachments/'.$productImage->path);
                $productImage->path=$fileNameToStore01;
                $productImage->save();
            }else{
                $productImage=new ProductAttachment();
                $productImage->id=CommonFunc::getPrimaryKey();
                $productImage->path=$fileNameToStore01;
                $productImage->product_id=$product->id;
                $productImage->attachment_types_id=AttachmentType::ProductPhoto;
                $productImage->status=StatusType::Active;
                $productImage->save();
            }
        }

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $productUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $productUnit)
    {
        //
    }

    public function getUnitById($id)
    {
        return Product::find($id)->productUnit;
    }
}
