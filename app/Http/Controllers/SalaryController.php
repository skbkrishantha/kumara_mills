<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\UserAccount;
use App\Model\Salary;
use App\Model\SalaryPeriod;
use App\Model\Employee;
use App\Model\Settings;
use App\Model\CashBook;
use App\Model\Role;
use App\Model\SalaryAttachment;
use App\Model\IdentityCardType;

use App\Type\StatusType;
use App\Type\AttachmentType;
use App\Type\CommonType;
use App\Type\SettingsType;
use App\Type\TransactionType;
use App\Type\ModuleType;

use App\Util\CommonFunc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

use PDF;

class SalaryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Salary::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }
        
        $salaries = Salary::select('tbl_salary.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('tbl_salary.NAME', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('tbl_salary.STATUS', '=',$request->input('search-status') )
                ->orderBy('INSERT_DATETIME', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('salary.list')->with('salaries',$salaries)
                                 ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Salary::class);

        $salary = new Salary();
        $cashbook = new CashBook();
        $employee_list=Employee::orderBy('name','asc')->pluck('name','id');
        $salary_period_list=SalaryPeriod::orderBy('display_name','asc')->pluck('display_name','id');
        $epf_employee_contrib_precentage=Settings::all()->where('id', '=',SettingsType::EPF_Employee_Contribution_Precentage)->first();
        $epf_company_contrib_precentage=Settings::all()->where('id', '=',SettingsType::EPF_Company_Contribution_Precentage)->first();
        $etf_company_contrib_precentage=Settings::all()->where('id', '=',SettingsType::ETF_Company_Contribution_Precentage)->first();

        return view('salary.create')->with('salary',$salary)
                                    ->with('cashbook',$cashbook)
                                    ->with('employee_list',$employee_list)                                    
                                    ->with('salary_period_list',$salary_period_list)
                                    ->with('epf_employee_contrib_precentage',$epf_employee_contrib_precentage)
                                    ->with('epf_company_contrib_precentage',$epf_company_contrib_precentage)
                                    ->with('etf_company_contrib_precentage',$etf_company_contrib_precentage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Salary::class);      

        $basic_salary=str_replace( ',', '', $request->input('basic_salary'));
        $request->merge(['basic_salary' => $basic_salary]);
        $this->validate($request,[      
            'employee_id' => 'required', 
            'salary_period_id' => 'required', 
            'epf_employee_contrib_precentage' => 'required|numeric',
            'epf_company_contrib_precentage' => 'required|numeric',
            'etf_company_contrib_precentage' => 'required|numeric', 
            'basic_salary' => 'required|numeric',           
        ]);
        
        $salary = new Salary;
        $salary->id=CommonFunc::getPrimaryKey();
        $salary->employee_id = $request->input('employee_id');
        $salary->salary_period_id = $request->input('salary_period_id');
        $salary->epf_employee_contrib_precentage=$request->input('epf_employee_contrib_precentage');
        $salary->epf_company_contrib_precentage=$request->input('epf_company_contrib_precentage');
        $salary->etf_company_contrib_precentage=$request->input('etf_company_contrib_precentage');        
        $salary->basic_salary=$request->input('basic_salary');
        
        $salary->total_for_epf_etf=$salary->basic_salary;
        $salary->total_earnings=$salary->basic_salary;        
        $salary->epf_employee_contrib=$salary->basic_salary*($salary->epf_employee_contrib_precentage/100);
        $salary->epf_company_contrib=$salary->basic_salary*($salary->epf_company_contrib_precentage/100);
        $salary->etf_company_contrib=$salary->basic_salary*($salary->etf_company_contrib_precentage/100); 
        $salary->total_for_tax=$salary->basic_salary;              
        $salary->total_deductions=$salary->epf_employee_contrib;  
        $salary->net_salary=$salary->basic_salary - $salary->total_deductions;

        $salary->comment=$request->input('comment');
        $salary->status=StatusType::Active;

        $cashbook = new CashBook;
        $cashbook->id=CommonFunc::getPrimaryKey();
        $cashbook->amount = $salary->total_earnings + $salary->epf_company_contrib + $salary->etf_company_contrib;
        $cashbook->payment_type = $request->input('payment_type');
        $cashbook->reference_no=$request->input('reference_no');
        $cashbook->effective_date=$request->input('effective_date');
        $cashbook->comment=$request->input('cashbook_comment');
        $cashbook->transaction_type=TransactionType::Credit;
        $cashbook->status=StatusType::Active;        
        $cashbook->module_type = ModuleType::Salary;

        DB::beginTransaction();
        try{
            $salary->save();
            $cashbook->reference_id = $salary->id;
            $cashbook->save();

            DB::commit();
            $msg = "Transaction successfully completed!";
            $msgType= "success";
        } catch (Exception $e) {
            DB::rollBack();
            $msg = "Transaction failed!";
            $msgType= "error";
        }

        return redirect()->route('salary.index')
                         ->with($msgType,$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $salary)
    {
        $this->authorize('view', Salary::class);
        return view('salary.view')->with('salary',$salary);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $salary)
    {
        $this->authorize('update', Salary::class);

        $cashbook = $salary->cashbook->first();
        $employee_list=Employee::orderBy('name','asc')->pluck('name','id');
        $salary_period_list=SalaryPeriod::orderBy('display_name','asc')->pluck('display_name','id');
        $epf_employee_contrib_precentage=Settings::all()->where('id', '=',SettingsType::EPF_Employee_Contribution_Precentage)->first();
        $epf_company_contrib_precentage=Settings::all()->where('id', '=',SettingsType::EPF_Company_Contribution_Precentage)->first();
        $etf_company_contrib_precentage=Settings::all()->where('id', '=',SettingsType::ETF_Company_Contribution_Precentage)->first();

        $epf_employee_contrib_precentage=$salary->epf_employee_contrib_precentage;
        $epf_company_contrib_precentage=$salary->epf_company_contrib_precentage;
        $etf_company_contrib_precentage=$salary->etf_company_contrib_precentage;

        return view('salary.create')->with('salary',$salary)
                                    ->with('cashbook',$cashbook)
                                    ->with('employee_list',$employee_list)                                    
                                    ->with('salary_period_list',$salary_period_list)
                                    ->with('epf_employee_contrib_precentage',$epf_employee_contrib_precentage)
                                    ->with('epf_company_contrib_precentage',$epf_company_contrib_precentage)
                                    ->with('etf_company_contrib_precentage',$etf_company_contrib_precentage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $salary)
    {
        $this->authorize('update', Salary::class);      

        $basic_salary=str_replace( ',', '', $request->input('basic_salary'));
        $request->merge(['basic_salary' => $basic_salary]);
        $this->validate($request,[      
            'employee_id' => 'required', 
            'salary_period_id' => 'required', 
            'epf_employee_contrib_precentage' => 'required|numeric',
            'epf_company_contrib_precentage' => 'required|numeric',
            'etf_company_contrib_precentage' => 'required|numeric', 
            'basic_salary' => 'required|numeric',           
        ]);
        
        $salary->employee_id = $request->input('employee_id');
        $salary->salary_period_id = $request->input('salary_period_id');
        $salary->epf_employee_contrib_precentage=$request->input('epf_employee_contrib_precentage');
        $salary->epf_company_contrib_precentage=$request->input('epf_company_contrib_precentage');
        $salary->etf_company_contrib_precentage=$request->input('etf_company_contrib_precentage');        
        $salary->basic_salary=$request->input('basic_salary');
        
        $salary->total_for_epf_etf=$salary->basic_salary;
        $salary->total_earnings=$salary->basic_salary;        
        $salary->epf_employee_contrib=$salary->basic_salary*($salary->epf_employee_contrib_precentage/100);
        $salary->epf_company_contrib=$salary->basic_salary*($salary->epf_company_contrib_precentage/100);
        $salary->etf_company_contrib=$salary->basic_salary*($salary->etf_company_contrib_precentage/100); 
        $salary->total_for_tax=$salary->basic_salary;              
        $salary->total_deductions=$salary->epf_employee_contrib;  
        $salary->net_salary=$salary->basic_salary - $salary->total_deductions;

        $salary->comment=$request->input('comment');
        $salary->status=StatusType::Active;

        $cashbook = $salary->cashbook->first();
        $cashbook->amount = $salary->total_earnings + $salary->epf_company_contrib + $salary->etf_company_contrib;
        $cashbook->payment_type = $request->input('payment_type');
        $cashbook->reference_no=$request->input('reference_no');
        $cashbook->effective_date=$request->input('effective_date');
        $cashbook->comment=$request->input('cashbook_comment');
        $cashbook->transaction_type=TransactionType::Credit;
        $cashbook->status=StatusType::Active;        
        $cashbook->module_type = ModuleType::Salary;

        DB::beginTransaction();
        try{
            $salary->save();
            $cashbook->reference_id = $salary->id;
            $cashbook->save();

            DB::commit();
            $msg = "Transaction successfully completed!";
            $msgType= "success";
        } catch (Exception $e) {
            DB::rollBack();
            $msg = "Transaction failed!";
            $msgType= "error";
        }

        return redirect()->route('salary.index')
                         ->with($msgType,$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salary $salary)
    {
        //
    }

    public function print($id)
    {
        $this->authorize('view', Salary::class);
        
        $salary =Salary::find($id);

        $pdf = PDF::loadView('salary.print', ['salary'=>$salary]);
        return $pdf->setPaper('a4')->setWarnings(false)->stream();
    }
}
