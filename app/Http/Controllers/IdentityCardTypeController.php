<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\IdentityCardType;

use App\Model\Settings;
use App\Util\CommonFunc;
use App\Type\StatusType;

class IdentityCardTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Settings::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $identity_card_types = IdentityCardType::select('mst_identity_card_types.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('mst_identity_card_types.name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('mst_identity_card_types.status', '=',$request->input('search-status') )
                ->orderBy('insert_datetime', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('identity_card_type.list')->with('identity_card_types',$identity_card_types)
                                              ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Settings::class);

        $identity_card_type = new IdentityCardType();

        return view('identity_card_type.create')->with('identity_card_type',$identity_card_type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_identity_card_types',
            'display_name' => 'required|unique:mst_identity_card_types',              
        ]);

        $identity_card_type= new IdentityCardType;
        $identity_card_type->id=CommonFunc::getPrimaryKey();
        $identity_card_type->name=$request->input('name');
        $identity_card_type->display_name=$request->input('display_name');
        $identity_card_type->status=StatusType::Active;
               
        $identity_card_type->save();

        return redirect()->route('identity_card_type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Settings::class);

        $identity_card_type = IdentityCardType::find($id);

        return view('identity_card_type.view')->with('identity_card_type',$identity_card_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Settings::class);

        $identity_card_type = IdentityCardType::find($id);

        return view('identity_card_type.create')->with('identity_card_type',$identity_card_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_identity_card_types,name,'.$id,
            'display_name' => 'required|unique:mst_identity_card_types,display_name,'.$id,              
        ]);

        $identity_card_type= IdentityCardType::find($id);
        $identity_card_type->name=$request->input('name');
        $identity_card_type->display_name=$request->input('display_name');
               
        $identity_card_type->save();

        return redirect()->route('identity_card_type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
