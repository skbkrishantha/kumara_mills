<?php

namespace App\Http\Controllers\Auth;

use App\Model\UserAccount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

use App\Util\CommonFunc;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:tbl_user_account',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {     
        $user=Auth::user();
        $user_id=null;
               
        if($user!= null){
            $user_id=$user->id;
        }
        
        $userAccount=UserAccount::create([
            'id' => CommonFunc::getPrimaryKey(),
            'user_name' => $data['name'],
            'email' => $data['email'],
            'user_password' => Hash::make($data['password']),
            'status' => 0,
            'insert_user_id' => $user_id,
            'update_user_id' => $user_id,
            'role_id' => $data['role'],
        ]);

        return $userAccount;
    }
}
