<?php

namespace App\Http\Controllers;

use App\Model\SalaryPeriod;
use Illuminate\Http\Request;
use App\Model\Settings;
use App\Util\CommonFunc;
use App\Type\StatusType;

class SalaryPeriodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Settings::class);

        if($request['search-status'] == null){
            $request['search-status'] =StatusType::Active;
        }     
        
        $salary_periods = SalaryPeriod::select('mst_salary_periods.*')
                ->when(!empty($request->input('search-name')) , function ($query) use($request){
                    return $query->where('mst_salary_periods.name', 'like','%'. $request->input('search-name').'%' );
                })
                ->where('mst_salary_periods.status', '=',$request->input('search-status') )
                ->orderBy('insert_datetime', 'DESC')            
                ->paginate(CommonFunc::getPaginationSize());

        return view('salary_period.list')->with('salary_periods',$salary_periods)
                                     ->with('request',$request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Settings::class);

        $salary_period = new SalaryPeriod();

        return view('salary_period.create')->with('salary_period',$salary_period);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_salary_periods',
            'display_name' => 'required|unique:mst_salary_periods',              
        ]);

        $salary_period= new SalaryPeriod;
        $salary_period->id=CommonFunc::getPrimaryKey();
        $salary_period->name=$request->input('name');
        $salary_period->display_name=$request->input('display_name');
        $salary_period->status=StatusType::Active;
               
        $salary_period->save();

        return redirect()->route('salary_period.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalaryPeriod  $salaryPeriod
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryPeriod $salaryPeriod)
    {
        $this->authorize('view', Settings::class);

        return view('salary_period.view')->with('salary_period',$salaryPeriod);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalaryPeriod  $salaryPeriod
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryPeriod $salaryPeriod)
    {
        $this->authorize('view', Settings::class);

        return view('salary_period.create')->with('salary_period',$salaryPeriod);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalaryPeriod  $salaryPeriod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryPeriod $salary_period)
    {
        $this->authorize('create', Settings::class);

        $this->validate($request,[
            'name' => 'required|unique:mst_salary_periods,name,'.$salary_period->id,
            'display_name' => 'required|unique:mst_salary_periods,display_name,'.$salary_period->id,              
        ]);

        $salary_period->name=$request->input('name');
        $salary_period->display_name=$request->input('display_name');
        $salary_period->status=StatusType::Active;
        $salary_period->save();

        return redirect()->route('salary_period.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalaryPeriod  $salaryPeriod
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryPeriod $salaryPeriod)
    {
        //
    }
}
