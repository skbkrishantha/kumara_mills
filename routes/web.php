<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('login/locked', 'Auth\LoginController@locked')->middleware('auth')->name('login.locked');
Route::post('login/locked', 'Auth\LoginController@unlock')->name('login.unlock');

Route::get('/',[
    'as' => 'index',
    'uses' => 'HomeController@index'
]);

Route::resource('employee','EmployeeController');

Route::get('settings',[
    'as' => 'settings',
    'uses' => 'SettingsController'
]);

Route::resource('user_role','UserRoleController');

Route::resource('module_permission','ModulePermissionController');

Route::resource('client','ClientController');

Route::resource('identity_card_type', 'IdentityCardTypeController');

Route::get('company',[
    'as' => 'company.show',
    'uses' => 'CompanyController@show'
]);

Route::resource('event', 'EventController', [
    'except' => [ 'show']
]);

Route::get('event/delete_event/{id}',[
    'as' => 'event.delete_event',
    'uses' => 'EventController@delete'
]);

Route::get('event/calander_view',[
    'as' => 'event.calander_view',
    'uses' => 'EventController@calander_view'
]);

Route::resource('product_unit', 'ProductUnitController');
Route::resource('product', 'ProductController');
Route::resource('supplier', 'SupplierController');
Route::resource('supply', 'SupplyController');
Route::resource('sell', 'SellController');

Route::resource('cashbook', 'CashBookController')->only([
    'index'
]);

Route::resource('stock', 'StockController')->only([
    'index'
]);

Route::get('stock/get_stock_by_product_id/{id}',[
    'as' => 'stock.get_stock_by_product_id',
    'uses' => 'StockController@getStockByProductId'
]);

Route::get('product/get_unit_by_id/{id}',[
    'as' => 'product.get_unit_by_id',
    'uses' => 'ProductController@getUnitById'
]);

Route::get('stock/stock_exists',[
    'as' => 'stock.stock_exists',
    'uses' => 'StockController@stockExists'
]);

Route::get('sell/{id}/print',[
    'as' => 'sell.print',
    'uses' => 'SellController@print'
]);

Route::get('stock/out_add_to_cart',[
    'as' => 'stock.out_add_to_cart',
    'uses' => 'StockController@outAddToCart'
]);

Route::get('stock/in_add_to_cart',[
    'as' => 'stock.in_add_to_cart',
    'uses' => 'StockController@inAddToCart'
]);

Route::get('stock/remove_from_cart/{id}',[
    'as' => 'stock.remove_from_cart',
    'uses' => 'StockController@removeFromCart'
]);

Route::get('stock/stock_check/{id}',[
    'as' => 'stock.stock_check',
    'uses' => 'StockController@getStockUnits'
]);

Route::get('stock/dashboard',[
    'as' => 'stock.dashboard',
    'uses' => 'StockController@dashboard'
]);

Route::get('stock/sell-graph-data',[
    'as' => 'stock.sell-graph-data',
    'uses' => 'StockController@sellGraphData'
]);


Route::resource('salary', 'SalaryController');
Route::resource('salary_period', 'SalaryPeriodController');

Route::get('salary/{id}/print',[
    'as' => 'salary.print',
    'uses' => 'SalaryController@print'
]);

Route::get('supply/{id}/print',[
    'as' => 'supply.print',
    'uses' => 'SupplyController@print'
]);
