@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Company
            <small>Handle all Company resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Company</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Company Deatails of :: <b>{{ $company->name }} </b> | <b>{{ $company->hotline}}</b></h3>   
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $company->name }}</td>
                                </tr>
                                <tr>
                                    <td>Address: </td>
                                    <td>{{ $company->address }}</td>
                                </tr>
                                <tr>
                                    <td>Telephone: </td>
                                    <td>{{ $company->telephone }}</td>
                                </tr>
                                <tr>
                                    <td>Hotline: </td>
                                    <td>{{ $company->hotline }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile: </td>
                                    <td>{{ $company->mobile }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody> 
                                <tr>
                                    <td>Email: </td>
                                    <td>{{ $company->email }}</td>
                                </tr>
                                <tr>
                                    <td>Fax: </td>
                                    <td>{{ $company->fax }}</td>
                                </tr> 
                                <tr>
                                    <td>Web Site: </td>
                                    <td>{{ $company->web }}</td>
                                </tr>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    
<script type="text/javascript">
    $("#company").addClass("active");
</script>
@endsection