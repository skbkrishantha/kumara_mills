@extends('layouts.error')

@section('content')
<div class="register-box">
        <div class="register-logo">
            <a href="{{ route('index') }}"><b>{{ config('app.name') }}</b></a>
        </div>
        <div class="register-box-body">
            <div class="text-center">
                    <h2>OOPS...</h2>
                    <h1> ERROR 404</h1>
                    <h3>{{ $exception->getMessage() }}</h3>
                    <a href="{{ url()->previous() }}" class="btn btn-default">BACK</a>
                    <a href="{{ route('index') }}" class="btn btn-default">HOME</a>
                    <br><br>
            </div> 
        </div>
</div> 
@endsection        