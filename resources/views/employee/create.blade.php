@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Employee
            <small>Handle all Employee resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('employee.index') }}"><i class="fa fa-users"></i> Employees</a></li>
            <li class="active">Add Employee</li>
        </ol>
</section>
<section class="content">
    <div class="container box">
            @if(!isset($user_account->id))
            <div class="box-header">
                <h3 class="box-title">Employee Basic Information</h3>
            </div>
            <div class="box-body">
                    {!! Form::open(['action' => 'EmployeeController@store','method' => 'POST','enctype' => 'multipart/form-data']) !!}
            @else
            <div class="box-header">
                <h3 class="box-title">Edit Employee Basic Information of :: <b>{{ $employee->name }} </b></h3>
            </div>
            <div class="box-body">
                    {!! Form::open(['route' => ['employee.update',$user_account->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                    {!! Form::hidden('_method','PUT') !!}
            @endif
                <div class='form-group row'>
                    {{Form::label('name','Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('name', $employee->name ,['class' => 'form-control','placeholder'=>'Name','required'=> 'true'])}}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>              

                <div class='form-group row'>
                    {{Form::label('employee_number','Emplyee Number',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('employee_number', $employee->employee_number ,['class' => 'form-control','placeholder'=>'Emplyee Number','required'=> 'true'])}}
                        @if ($errors->has('employee_number'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('employee_number') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class="form-group row">
                    {{Form::label('identity_card_type_id','Identity Card Type',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('identity_card_type_id', $identity_card_list, $employee->identity_card_type_id, ['placeholder' => 'Please select ...','class' => 'form-control'])!!}
                        @if ($errors->has('identity_card_type_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('identity_card_type_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class='form-group row'>
                    {{Form::label('identity_card_number','Identity Card Number',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('identity_card_number', $employee->identity_card_number ,['class' => 'form-control','placeholder'=>'Identity Card Number','required'=> 'true'])}}
                        @if ($errors->has('identity_card_number'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('identity_card_number') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('mobile1','Mobile',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('mobile1', $employee->mobile1 ,['class' => 'form-control','placeholder'=>'Mobile','required'=> 'true'])}}
                        @if ($errors->has('mobile1'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('mobile1') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('address1','Address',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('address1', $employee->address_01 ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Address','rows'=>4,'required'=> 'true'])}}
                        @if ($errors->has('address1'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('address1') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('joined_date','Joined Date',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group date" data-date-format="yyyy-mm-dd" id="datetimepicker3">
                            {{Form::text('joined_date', (!$employee->joined_date) ? $employee->joined_date : $employee->joined_date->format('Y-m-d')  ,['class' => 'form-control','required'=> 'true','placeholder' => 'Joined Date','readonly'])}}
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        
                        @if ($errors->has('joined_date'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('joined_date') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>
                <div class='form-group row'>
                    {{Form::label('image-01','Photo',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">                                              
                        <img id="image01" src="{{route('index')}}/storage/images/employee_attachments/{{ $employee->employee_photo()->path }}" class="imagePreview"/>  
                        {{ Form::file('image-01',['accept' => 'image/*','onchange' => 'readURL(this,"#image01")']) }}
                        @if ($errors->has('image-01'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('image-01') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class='form-group row'>
                    {{Form::label('comment','Comment',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $employee->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>
            </div>
    </div>            
</section>
<section class="content">
    <div class="container box">
            @if(!isset($user_account->id))
            <div class="box-header">
                <h3 class="box-title">Employee Login Information</h3>
            </div>
            <div class="box-body">
            @else
            <div class="box-header">
                <h3 class="box-title">Edit Employee Login Information</h3>
            </div>
            <div class="box-body">
            @endif
                <div class='form-group row'>
                    {{Form::label('user_name','User Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('user_name', $user_account->user_name ,['class' => 'form-control','placeholder'=>'User Name','required'=> 'true'])}}
                        @if ($errors->has('user_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('user_name') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('email','Email',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::email('email', $user_account->email ,['class' => 'form-control','placeholder'=>'User Email','required'=> 'true'])}}
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>
                @if(Auth::user()->id == $user_account->id || !isset($user_account->id))
                <div class="form-group row">
                    {{Form::label('password','Password',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">                        
                        {{Form::password('password',['class' => 'form-control','placeholder'=>'Password'])}}
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {{Form::label('password-confirm','Confirm Password',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::password('password_confirmation',['class' => 'form-control','placeholder'=>'Confirm Password'])}}
                    </div>
                </div>
                @endif
                
                <div class="form-group row">
                    {{Form::label('role','Role',['class' => 'col-md-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('role', $role_list, $user_account->role_id , ['class' => 'form-control role','placeholder' => 'Please Select','required'=>'true'])!!}
                        @if ($errors->has('role'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>                
                @if(isset($user_account->id))
                <script>
                    $('#role').each(function(){
                        $('#role option').each(function() {
                            if(!this.selected) {
                                $(this).attr('disabled', true);
                                $(this).hide();
                            }
                        });
                    });
                </script> 
                @endif  
            </div>
    </div>            
</section>

<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',['class' => 'btn btn-default'])}}
            {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#employees").addClass("active");

    $('#datetimepicker3').datetimepicker({
        format:'yyyy-mm-dd',
        pickTime: false,
        minView: 2,
        autoclose: 1,
    });
</script>

@endsection