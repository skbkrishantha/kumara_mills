@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Employee
            <small>Handle all Employee resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('employee.index') }}"><i class="fa fa-users"></i> Employee</a></li>
            <li class="active">Employees List</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Employee List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'employee.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-role','Role',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-3">
                        {!!Form::select('search-role',$role_list, $request->input('search-role'), ['placeholder' => 'Please select ...','class' => 'form-control'])!!}
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($employees) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Employee Number</th>
                        <th>Identity</th>
                        <th>Mobile</th>
                        <th>Address</th>
                        <th>Role</th>
                        <th>Joined Date</th>
                        <th class="bigColumn">Comment</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ (($employees->currentpage()-1) * $employees->perpage()) + $i++  }}</td>
                        <td class="bigColumn">{{$employee->name}}</td>
                        <td>{{$employee->employee_number}}</td>
                        <td>{{$employee->DisplayIdentityCard}}</td>
                        <td>{{$employee->mobile1}}</td>
                        <td class="bigColumn">{{$employee->address_01}}</td>
                        <td>{{$employee->user_account->role->display_name}}</td>
                        <td>{{$employee->JoinedDateFormated}}</td>
                        <td class="bigColumn">{{$employee->comment}}</td>
                        <td title="{{ $employee->recordBy()}}">
                            <a href="{{ route('employee.show', ['id' => $employee->user_account->id]) }}" class="btn btn-info btn-xs">
                                <i class="fa fa-info-circle"></i> View
                            </a>
                            <a href="{{ route('employee.edit', ['id' => $employee->user_account->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </a>
                        </td>                       
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $employees->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#employees").addClass("active");
</script>
@endsection