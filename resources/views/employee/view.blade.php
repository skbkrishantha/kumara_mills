@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Employee
            <small>Handle all Employee resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('employee.index') }}"><i class="fa fa-users"></i> Employees</a></li>
            <li class="active">View Employee</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Employee Deatails of :: <b>{{ $employee->name }} </b> | <b>{{ $user_account->role->display_name}}</b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('employee.edit', ['id' => $user_account->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                    <!-- Account Delete Should go to Settings
                    {!! Form::open(['route' => ['employee.destroy',$user_account->id], 'method' => 'POST','class'=> 'deleteForm']) !!}
                        {{Form::hidden('_method','DELETE')}}
                        {{Form::submit('Delete',['class'=> 'btn btn-danger btn-xs deleteBtn','data-toggle'=>'confirmation'])}}
                    {!! Form::close() !!}
                    -->
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-12" style="margin: 10px 0px;">
                    <img id="image01" src="{{route('index')}}/storage/images/employee_attachments/{{ $employee->employee_photo()->path }}" class="imagePreview"/> 
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $employee->name }}</td>
                                </tr>
                                <tr>
                                    <td>Employee Number: </td>
                                    <td>{{ $employee->employee_number }}</td>
                                </tr>
                                <tr>
                                    <td>Identity Card Deatails: </td>
                                    <td>{{ $employee->DisplayIdentityCard }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile: </td>
                                    <td>{{ $employee->mobile1 }}</td>
                                </tr>
                                <tr>
                                    <td>Address: </td>
                                    <td>{{ $employee->address_01 }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody> 
                                <tr>
                                    <td>Joined Date: </td>
                                    <td>{{ $employee->joined_date }}</td>
                                </tr>
                                <tr>
                                    <td>Comment: </td>
                                    <td>{{ $employee->comment }}</td>
                                </tr>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section> 
    
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Employee Login Information</h3>
            </div>            
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $user_account->user_name }}</td>
                                </tr>
                                <tr>
                                    <td>Email: </td>
                                    <td>{{ $user_account->email }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div><div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $user_account->role->display_name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>
        </div>            
</section>

<script type="text/javascript">
    $("#employees").addClass("active");
</script>
@endsection