<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }} - Dashboard @yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/css/AdminLTE.min.css') }}" rel="stylesheet">
  <link href="{{ asset('dist/css/skins/_all-skins.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('bower_components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/alertify.core.css') }}" rel="stylesheet">
  <link href="{{ asset('css/alertify.bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script type="text/javascript" src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('bower_components/bootstrap-confirmation/popper.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/alertify.min.js') }}"></script>  

</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ route('index') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>M</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Kumara-Mills</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{route('index')}}/storage/images/employee_attachments/{{ Auth::user()->employee != null ? Auth::user()->employee->employee_photo()->path : 'avatar-user.png' }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->user_name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{route('index')}}/storage/images/employee_attachments/{{ Auth::user()->employee != null ? Auth::user()->employee->employee_photo()->path : 'avatar-user.png' }}" class="img-circle" alt="User Image">
                <p>
                  {{ Auth::user()->user_name }}
                  <small>{{ Auth::user()->role->display_name }}</small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ route('employee.show', ['id' => Auth::user()->id]) }}" class="btn btn-default btn-flat">
                      Profile
                  </a>
                </div>
                
                <div class="pull-right">
                  <a href="{{route('index')}}" class="btn btn-default btn-flat"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                      Sign out
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" id="user-panel">
        <div class="pull-left image">
          <img src="{{route('index')}}/storage/images/employee_attachments/{{ Auth::user()->employee != null ? Auth::user()->employee->employee_photo()->path : 'avatar-user.png' }}" class="user-image" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->user_name }}</p>
          <small>{{ Auth::user()->role->display_name }}</small>
        </div>
      </div>
      <!-- search form -->
	  <!--
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->                                    
      <ul class="sidebar-menu" data-widget="tree">
        <!--<li class="header">MAIN NAVIGATION</li>-->        
		    <li id="dashboard">
          <a href="{{ route('index') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>            
          </a>
        </li>
        @can('visible', App\Model\Supply::class)
        <li id="supply" class="treeview">
          <a href="#">
              <i class="fa fa-arrow-right"></i> <span>Supply</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{ route('supply.index') }}"><i class="fa fa-list"></i> List</a></li>  
              <li><a href="{{ route('supply.create') }}"><i class="fa fa-plus"></i> New</a></li>
          </ul>
        </li> 
        @endcan
        @can('visible', App\Model\Sell::class)
        <li id="sell" class="treeview">
          <a href="#">
              <i class="fa fa-arrow-left"></i> <span>Sell</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{ route('sell.index') }}"><i class="fa fa-list"></i> List</a></li>  
              <li><a href="{{ route('sell.create') }}"><i class="fa fa-plus"></i> New</a></li>
          </ul>
        </li> 
        @endcan
        @can('visible', App\Model\Stock::class)
        <li id="stock">
          <a href="{{ route('stock.dashboard') }}">
            <i class="fa fa-cubes"></i> <span>Stock</span>            
          </a>
        </li>
        @endcan
        @can('visible', App\Model\CashBook::class)
        <li id="cashbook">
          <a href="{{ route('cashbook.index') }}">
            <i class="fa fa-money"></i> <span>CashBook</span>            
          </a>
        </li>
        @endcan
        @can('visible', App\Model\Product::class)
        <li id="product" class="treeview">
          <a href="#">
              <i class="fa fa-product-hunt"></i> <span>Products</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{ route('product.index') }}"><i class="fa fa-list"></i> List</a></li>  
              <li><a href="{{ route('product.create') }}"><i class="fa fa-plus"></i> New</a></li>
          </ul>
        </li> 
        @endcan
        @can('visible', App\Model\Client::class)
        <li id="client" class="treeview">
          <a href="#">
              <i class="fa fa-smile-o"></i> <span>Clients</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{ route('client.index') }}"><i class="fa fa-list"></i> List</a></li>  
              <li><a href="{{ route('client.create') }}"><i class="fa fa-plus"></i> New</a></li>
          </ul>
        </li> 
        @endcan
        @can('visible', App\Model\Supplier::class)
        <li id="supplier" class="treeview">
          <a href="#">
              <i class="fa fa-truck"></i> <span>Suppliers</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{ route('supplier.index') }}"><i class="fa fa-list"></i> List</a></li>  
              <li><a href="{{ route('supplier.create') }}"><i class="fa fa-plus"></i> New</a></li>
          </ul>
        </li> 
        @endcan
        @can('visible', App\Model\Salary::class)
        <li id="salaries" class="treeview">
            <a href="">
                <i class="fa fa-usd"></i> <span>Salary</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('salary.index') }}"><i class="fa fa-list"></i> List</a></li>  
                <li><a href="{{ route('salary.create') }}"><i class="fa fa-plus"></i> New</a></li>
            </ul>
        </li>
        @endcan
        @can('visible', App\Model\Employee::class)
        <li id="employees" class="treeview">
            <a href="">
                <i class="fa fa-users"></i> <span>Employees</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('employee.index') }}"><i class="fa fa-list"></i> List</a></li>  
                <li><a href="{{ route('employee.create') }}"><i class="fa fa-plus"></i> New</a></li>
            </ul>
        </li>
        @endcan
        @can('visible', App\Model\Settings::class)         
        <li id="settings">
            <a href="{{ route('settings') }}">
              <i class="fa fa-cogs"></i> 
              <span>Settings</span>
            </a>
        </li>                
        @endcan
        <li id="event">
            <a href="{{ route('event.index') }}">
                <i class="fa fa-calendar"></i> <span>Events</span>
            </a>
        </li>
        <li id="company">
            <a href="{{ route('company.show') }}">
                <i class="fa fa-cogs" style="font-size: 16px; color:#1b926c"></i>
                <span>Company</span>
            </a>
        </li> 
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>  
  <div class="content-wrapper">
            @yield('content')
            <div class="container">
               @include('inc.messages')
            </div>
  </div>  

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <div class="text-center">
    Copyright &copy; 2019 <b>{{ config('app.name') }}<a href="" class="text-black"> DAIT SOLUTION</a></b>
      All rights reserved
    </div>
  </footer>
  
</div>
<!-- ./wrapper -->


<script type="text/javascript" src="{{ URL::asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('bower_components/raphael/raphael.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('bower_components/morris.js/morris.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('bower_components/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/adminlte.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.masknumber.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.config.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
  //Timepicker
  $('.timepicker').timepicker({
        showInputs: false,
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false,
        defaultTime: false,
        keepInvalid: true,
        dateLimit: { hours: 48}
      });
</script>

@yield('js')

</body>
</html>

