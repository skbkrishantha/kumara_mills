@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Client
                <small>Handle all Client Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="fa fa-smile-o"></i> Client</a></li>  
                <li class="active">List Client</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Client List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'client.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($clients) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Idenity</th>
                        <th>Mobile</th>
                        <th>Address</th>
                        <th>Joined Date</th>
                        <th>Comment</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                    <tr>
                        <td>{{ (($clients->currentpage()-1) * $clients->perpage()) + $i++  }}</td>
                        <td class="bigColumn">{{$client->name}}</td>
                        <td>{{$client->DisplayIdentityCard}}</td>
                        <td>{{$client->mobile1}}</td>
                        <td class="bigColumn">{{$client->address1}}</td>
                        <td>{{$client->JoinedDateFormated}}</td>
                        <td class="bigColumn">{{$client->comment}}</td>
                        <td title="{{ $client->recordBy()}}">
                            <a href="{{ route('client.show', ['id' => $client->id]) }}" class="btn btn-info btn-xs">
                                <i class="fa fa-info-circle"></i> View
                            </a>
                            <a href="{{ route('client.edit', ['id' => $client->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </a>
                        </td>                       
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $clients->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#client").addClass("active");
</script>
@endsection