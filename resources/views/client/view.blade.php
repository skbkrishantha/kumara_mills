@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Client
                <small>Handle all Client Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('client.index') }}"><i class="fa fa-smile-o"></i> Client</a></li>  
                <li class="active">List Client</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Client Deatails of :: <b>{{ $client->name }} </b> </h3>
                <div class="semi pull-right">
                    <a href="{{ route('client.edit', ['id' => $client->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-12" style="margin: 10px 0px;">
                    <img id="image01" src="{{route('index')}}/storage/images/client_attachments/{{ $client->client_photo()->path }}" class="imagePreview"/> 
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $client->name }}</td>
                                </tr>
                                <tr>
                                    <td>Identity Card Deatails: </td>
                                    <td>{{ $client->DisplayIdentityCard }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile: </td>
                                    <td>{{ $client->mobile1 }}</td>
                                </tr>
                                <tr>
                                    <td>Address: </td>
                                    <td>{{ $client->address1 }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody> 
                                <tr>
                                    <td>Joined Date: </td>
                                    <td>{{ $client->joined_date }}</td>
                                </tr>
                                <tr>
                                    <td>Comment: </td>
                                    <td>{{ $client->comment }}</td>
                                </tr>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section> 

<script type="text/javascript">
    $("#client").addClass("active");
</script>
@endsection