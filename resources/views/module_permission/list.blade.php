@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Module Permissions
            <small>Handle all Module Permissions</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li><a href="{{ route('module_permission.index') }}"><i class="fa fa-cogs"></i> Module Permissions</a></li>
            <li class="active">Module Permissions List</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Module Permissions List</h3> 
                <div class="semi pull-right">
                    <a href="{{ route('module_permission.create') }}" class="btn btn-success btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Add New
                    </a>
                </div>  
            </div>            
            <div class="box-body">
                {!! Form::open(['route' => 'module_permission.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($module_permissions) > 0)
                <div class="table-responsive" style="overflow: auto">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Module Name</th>
                            <th>Permission</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($module_permissions as $module_permission)
                        <tr>
                            <td>{{ (($module_permissions->currentpage()-1) * $module_permissions->perpage()) + $i++  }}</td>
                            <td>{{$module_permission->module_name}}</td>
                            <td>{{$module_permission->permission}}</td>
                            <td title="{{ $module_permission->recordBy()}}">
                                <a href="{{ route('module_permission.show', ['id' => $module_permission->id]) }}" class="btn btn-info btn-xs">
                                    <i class="fa fa-info-circle"></i> View
                                </a>
                                <a href="{{ route('module_permission.edit', ['id' => $module_permission->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                    <i class="fa fa-pencil-square-o"></i> Edit
                                </a>
                            </td>
                        </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
            <div class="centerBlock">
                {{ $module_permissions->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection