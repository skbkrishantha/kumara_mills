@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Module Permissions
            <small>Handle all Module Permissions</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li><a href="{{ route('module_permission.index') }}"><i class="fa fa-cogs"></i> Module Permissions</a></li>
            <li class="active">Module Permissions View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            @if(!isset($module_permission->id))
            <div class="box-header">
                <h3 class="box-title">Module Permissions Basic Information</h3>
            </div>
            <div class="box-body">
                    {!! Form::open(['route' => ['module_permission.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
            @else
            <div class="box-header">
                <h3 class="box-title">Edit Module Permissions Basic Information of :: <b>{{ $module_permission->module_name }} | {{ $module_permission->permission }} </b></h3>
            </div>
            <div class="box-body">
                    {!! Form::open(['route' => ['module_permission.update',$module_permission->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                    {!! Form::hidden('_method','PUT') !!}
            @endif
                <div class='form-group row'>
                    {{Form::label('module_name','Module Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::select('module_name', App\Type\ModuleType::getKeyValue(),$module_permission->module_name ,['class' => 'form-control','placeholder'=>'Please Select','required'=> 'true'])}}
                        @if ($errors->has('module_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('module_name') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('permission','Permission',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::select('permission', App\Type\ModulePermissionType::getKeyValue(), $module_permission->permission ,['class' => 'form-control','placeholder'=>'Please Select','required'=> 'true'])}}
                        @if ($errors->has('permission'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('permission') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>
            </div>               
        </div>            
    </section>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-12 col-sm-offset-1">
                {{Form::reset('Reset',['class' => 'btn btn-default'])}}
                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
            </div>
        </div>    
    </div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#settings").addClass("active");
</script>

@endsection