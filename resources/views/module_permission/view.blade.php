@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Module Permissions
            <small>Handle all Module Permissions</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li><a href="{{ route('module_permission.index') }}"><i class="fa fa-cogs"></i> Module Permissions</a></li>
            <li class="active">Module Permissions View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Module Permissions Deatails of :: <b>{{ $module_permission->module_name }} | <b>{{ $module_permission->permission }} </b></b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('module_permission.edit', ['id' => $module_permission->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Module Name: </td>
                                    <td>{{ $module_permission->module_name }}</td>
                                </tr>
                                <tr>
                                    <td>Permission: </td>
                                    <td>{{ $module_permission->permission }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody> 
                                                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection