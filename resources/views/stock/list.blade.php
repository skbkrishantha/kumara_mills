@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Stock
                <small>Handle all Stock Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('stock.dashboard') }}"><i class="fa fa-smile-o"></i> Stock</a></li>  
                <li class="active">Transactions</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Stock List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'stock.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    <!--
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>
                    -->

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($stocks) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Reference</th>
                        <th>Product Id</th>
                        <th>Units</th>
                        <th>Unit</th>   
                        <th>Unit Price</th>                      
                        <th>Module Type</th>
                        <th>Stock Type</th>
                        <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($stocks as $stock)
                    <tr>
                        <td>{{ (($stocks->currentpage()-1) * $stocks->perpage()) + $i++  }}</td>
                        <td>{{$stock->id}}</td>
                        <td>{{$stock->product->name}}</td>
                        <td>{{$stock->units}}</td>
                        <td>{{$stock->product->productUnit->display_name}}</td>
                        <td>{{\App\Util\CommonFunc::getPrefixCurrencyFormat($stock->unit_price)}}</td>
                        <td>{{$stock->module_type}}</td>
                        <td>{{$stock->stock_type}}</td>
                        <td>{{$stock->update_datetime}}</td>                      
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $stocks->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#stock").addClass("active");
</script>
@endsection