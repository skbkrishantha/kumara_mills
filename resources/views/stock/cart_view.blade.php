<section class="content">
    <div class="container box">
        <div class="box-header">
            <h3 class="box-title">Cart</h3>   
        </div>            
        <div class="box-body">
            @php ($i = 1)
            @if(count($carts) > 0)
            <!-- /.box-header -->
            <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Reference</th>
                            <th>Product Id</th>
                            <th>Units</th>
                            <th>Unit</th>
                            <th>Unit Price</th>
                            <th>Sub Total</th>
                            <th>Stock Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($carts as $cart)
                    <tr>
                        <td>{{ $i++  }}</td>
                        <td>{{$cart->id}}</td>
                        <td>{{$cart->product->name}}</td>
                        <td>{{$cart->units}}</td>
                        <td>{{$cart->product->productUnit->display_name}}</td>
                        <td align="right">{{\App\Util\CommonFunc::getPrefixCurrencyFormat($cart->unit_price)}}</td>
                        <td align="right">{{\App\Util\CommonFunc::getPrefixCurrencyFormat($cart->units * $cart->unit_price)}}</td>
                        <td>{{$cart->stock_type}}</td>  
                        <th>
                            <button type="button" id="{{$cart->id}}"
                                class="ibtnDel btn btn-danger btn-xs d-none">
                                <i class="fa fa-minus"></i>
                            </button>
                        </th>                    
                    </tr>
                    @endforeach 
                    <tr>
                        @php($total = 0.00)
                        @foreach($carts as $cart)
                        @php($total += $cart->units * $cart->unit_price)
                        @endforeach
                        <td colspan="7" align="right"><b>Total: </b>{{\App\Util\CommonFunc::getPrefixCurrencyFormat($total)}}</td> 
                        <span style="display:none" id="cart_total">{{\App\Util\CommonFunc::getCurrencyFormat($total)}}</span>                  
                    </tr>   
                    </tbody>
                </table>
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </div>
</section>

