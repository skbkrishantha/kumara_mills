@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Stock
                <small>Handle all Stock Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li> 
                <li class="active">Stock</li>    
            </ol>
    </section>
    <section class="content">    
        <div class="container box"> 
            <div class="box-header">
                <h3 class="box-title">Stock Listing</h3>
                <div class="semi pull-right">
                    <a href="{{ route('stock.index') }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Transactions
                    </a>
                </div>    
            </div>
            <div class="box-body">
                @foreach($stocks as $stock)
                <div class="col-md-3 center-block" style="margin-bottom: 30px;">
                    <figure class="card card-product"  style="background-color:#eee;">
                        <div class="img-wrap"><img class="img-circle imagePreview" style="margin-top:20px;" src="{{route('index')}}/storage/images/product_attachments/{{ $stock->product->product_photo()->path }}"></div>
                        <figcaption class="info-wrap">
                                <h4 class="title" align="center">{{ $stock->product->display_name  }} / {{ $stock->product->productUnit->display_name }}</h4>
                                <p class="desc" align="center">{{ $stock->product->material_type  }}</p>
                                <div class="rating-wrap" align="center">
                                    <div class="label-rating">{{ $stock->units }} units available</div>
                                </div> <!-- rating-wrap.// -->
                        </figcaption>
                        <div class="bottom-wrap" align="center">
                            <a href="{{ route('product.show', ['id' => $stock->product->id]) }}" class="btn btn-info">
                                <i class="fa fa-info-circle"></i> View Product
                            </a>
                        </div> <!-- bottom-wrap.// -->
                    </figure>      
                </div>
                @endforeach
            </div>
        </div>
    </section> 

<script type="text/javascript">
    $("#stock").addClass("active");
</script>
@endsection