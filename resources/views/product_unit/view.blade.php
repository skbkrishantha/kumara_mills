@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Product Units
            <small>Handle all Product Units resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li><a href="{{ route('product_unit.index') }}"><i class="fa fa-cogs"></i> Product Units</a></li>
            <li class="active">Product Unit View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Product Unit Deatails of :: <b>{{ $product_unit->name }} </b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('product_unit.edit', ['id' => $product_unit->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $product_unit->name }}</td>
                                </tr>
                                <tr>
                                    <td>Dispaly Name: </td>
                                    <td>{{ $product_unit->display_name }}</td>
                                </tr>
                                <tr>
                                    <td>Weight(KG): </td>
                                    <td>{{ $product_unit->weight }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection