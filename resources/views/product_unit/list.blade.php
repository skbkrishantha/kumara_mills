@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Product Units
            <small>Handle all Product Units resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li class="active">Product Units</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Product Unit List</h3> 
                <div class="semi pull-right">
                    <a href="{{ route('product_unit.create') }}" class="btn btn-success btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Add New
                    </a>
                </div>  
            </div>            
            <div class="box-body">
                {!! Form::open(['route' => 'product_unit.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                        {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($product_units) > 0)
                <div class="table-responsive" style="overflow: auto">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Weight(KG)</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($product_units as $product_unit)
                        <tr>
                            <td>{{ (($product_units->currentpage()-1) * $product_units->perpage()) + $i++  }}</td>
                            <td>{{$product_unit->name}}</td>
                            <td>{{$product_unit->display_name}}</td>
                            <td>{{$product_unit->weight}}</td>
                            <td title="{{ $product_unit->recordBy()}}">
                                <a href="{{ route('product_unit.show', ['id' => $product_unit->id]) }}" class="btn btn-info btn-xs">
                                    <i class="fa fa-info-circle"></i> View
                                </a>
                                <a href="{{ route('product_unit.edit', ['id' => $product_unit->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                    <i class="fa fa-pencil-square-o"></i> Edit
                                </a>
                            </td>
                        </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
                {{ $product_units->appends(Request::except('page'))->links() }}
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection