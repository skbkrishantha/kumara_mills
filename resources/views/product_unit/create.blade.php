@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Product Units
        <small>Handle all Product Units resources</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
        <li><a href="{{ route('product_unit.index') }}"><i class="fa fa-cogs"></i> Product Units</a></li>
        <li class="active">Product Unit Create</li>
    </ol>
</section>
<section class="content">
    <div class="container box">
        @if(!isset($product_unit->id))
        <div class="box-header">
            <h3 class="box-title">Product Unit Basic Information</h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['product_unit.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
        @else
        <div class="box-header">
            <h3 class="box-title">Edit Product Unit Basic Information of :: <b>{{ $product_unit->display_name }} </b></h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['product_unit.update',$product_unit->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                {!! Form::hidden('_method','PUT') !!}
        @endif
            <div class='form-group row'>
                {{Form::label('name','Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('name', $product_unit->name ,['class' => 'form-control','placeholder'=>'Name','required'=> 'true'])}}
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('display_name','Dispaly Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('display_name', $product_unit->display_name ,['class' => 'form-control','placeholder'=>'Display Name','required'=> 'true'])}}
                    @if ($errors->has('display_name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('display_name') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('weight','Weight(KG)',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::number('weight', $product_unit->weight ,['class' => 'form-control','placeholder'=>'Weight(KG)','required'=> 'true'])}}
                    @if ($errors->has('weight'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('weight') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

        </div>               
    </div>            
</section>

<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',['class' => 'btn btn-default'])}}
            {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#settings").addClass("active");
</script>

@endsection