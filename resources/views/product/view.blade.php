@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Products
            <small>Handle all Products resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('product.index') }}"><i class="fa fa-cogs"></i> Products</a></li>
            <li class="active">Product View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Product Deatails of :: <b>{{ $product->name }} </b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('product.edit', ['id' => $product->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </div>    
            </div>
            <div class="box-body">            
                <div class="col-md-12" style="margin: 10px 0px;">
                    <img id="image01" src="{{route('index')}}/storage/images/product_attachments/{{ $product->product_photo()->path }}" class="img-circle imagePreview"/> 
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $product->name }}</td>
                                </tr>
                                <tr>
                                    <td>Dispaly Name: </td>
                                    <td>{{ $product->display_name }}</td>
                                </tr>
                                <tr>
                                    <td>Current Unit Price: </td>
                                    <td>{{ \App\Util\CommonFunc::getPrefixCurrencyFormat($product->current_price) }}</td>
                                </tr>
                                <tr>
                                    <td>Weight(KG): </td>
                                    <td>{{ $product->productUnit->display_name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    

<script type="text/javascript">
    $("#product").addClass("active");
</script>
@endsection