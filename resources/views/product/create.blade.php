@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Products
        <small>Handle all Products resources</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('product.index') }}"><i class="fa fa-cogs"></i> Products</a></li>
        <li class="active">Product Create</li>
    </ol>
</section>
<section class="content">
    <div class="container box">
        @if(!isset($product->id))
        <div class="box-header">
            <h3 class="box-title">Product Basic Information</h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['product.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
        @else
        <div class="box-header">
            <h3 class="box-title">Edit Product Basic Information of :: <b>{{ $product->display_name }} </b></h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['product.update',$product->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                {!! Form::hidden('_method','PUT') !!}
        @endif
            <div class='form-group row'>
                {{Form::label('name','Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('name', $product->name ,['class' => 'form-control','placeholder'=>'Name','required'=> 'true'])}}
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('display_name','Dispaly Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('display_name', $product->display_name ,['class' => 'form-control','placeholder'=>'Display Name','required'=> 'true'])}}
                    @if ($errors->has('display_name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('display_name') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('current_price','Current Unit Price',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-3">
                    <div class="input-group">                            
                        <span class="input-group-addon">Rs.</span>                           
                        {{Form::text('current_price', $product->current_price  ,[  'class' => 'form-control currency-fommater','required'=> 'true'])}}
                    </div>
                    @if ($errors->has('current_price'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('current_price') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class="form-group row">
                {{Form::label('product_unit_id','Product Unit',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {!!Form::select('product_unit_id', $product_unit_list, $product->product_unit_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                    @if ($errors->has('product_unit_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('product_unit_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{Form::label('material_type','Meterial Type',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {!!Form::select('material_type', App\Type\MaterialType::getKeyValue(), $product->material_type, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                    @if ($errors->has('material_type'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('material_type') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class='form-group row'>
                {{Form::label('image-01','Photo',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-3">                                              
                    <img id="image01" src="{{route('index')}}/storage/images/product_attachments/{{ $product->product_photo()->path }}" class="img-circle imagePreview"/>                                             
                    {{ Form::file('image-01',['accept' => 'image/*','onchange' => 'readURL(this,"#image01")']) }}
                    @if ($errors->has('image-01'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('image-01') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

        </div>               
    </div>            
</section>

<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',['class' => 'btn btn-default'])}}
            {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#product").addClass("active");
</script>

@endsection