@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Products
            <small>Handle all Products resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Products</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Product List</h3> 
                <div class="semi pull-right">
                    <a href="{{ route('product.create') }}" class="btn btn-success btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Add New
                    </a>
                </div>  
            </div>            
            <div class="box-body">
                {!! Form::open(['route' => 'product.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                        {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($products) > 0)
                <div class="table-responsive" style="overflow: auto">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Product Unit</th>
                            <th>Current Unit Price</th>
                            <th>Material Type</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{ (($products->currentpage()-1) * $products->perpage()) + $i++  }}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->display_name}}</td>
                            <td>{{$product->productunit->name}}</td>
                            <td>{{\App\Util\CommonFunc::getPrefixCurrencyFormat($product->current_price)}}</td>
                            <td>{{$product->material_type}}</td>
                            <td title="{{ $product->recordBy()}}">
                                <a href="{{ route('product.show', ['id' => $product->id]) }}" class="btn btn-info btn-xs">
                                    <i class="fa fa-info-circle"></i> View
                                </a>
                                <a href="{{ route('product.edit', ['id' => $product->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                    <i class="fa fa-pencil-square-o"></i> Edit
                                </a>
                            </td>
                        </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
                {{ $products->appends(Request::except('page'))->links() }}
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#product").addClass("active");
</script>
@endsection