@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Supplier
                <small>Handle all Supplier Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('supplier.index') }}"><i class="fa fa-smile-o"></i> Supplier</a></li>  
                <li class="active">List Supplier</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Supplier List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'supplier.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($suppliers) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Idenity</th>
                        <th>Mobile</th>
                        <th>Address</th>
                        <th>Joined Date</th>
                        <th>Comment</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($suppliers as $supplier)
                    <tr>
                        <td>{{ (($suppliers->currentpage()-1) * $suppliers->perpage()) + $i++  }}</td>
                        <td class="bigColumn">{{$supplier->name}}</td>
                        <td>{{$supplier->DisplayIdentityCard}}</td>
                        <td>{{$supplier->mobile1}}</td>
                        <td class="bigColumn">{{$supplier->address1}}</td>
                        <td>{{$supplier->JoinedDateFormated}}</td>
                        <td class="bigColumn">{{$supplier->comment}}</td>
                        <td title="{{ $supplier->recordBy()}}">
                            <a href="{{ route('supplier.show', ['id' => $supplier->id]) }}" class="btn btn-info btn-xs">
                                <i class="fa fa-info-circle"></i> View
                            </a>
                            <a href="{{ route('supplier.edit', ['id' => $supplier->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </a>
                        </td>                       
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $suppliers->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#supplier").addClass("active");
</script>
@endsection