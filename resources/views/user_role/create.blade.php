@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        User Roles
        <small>Handle all User Roles resources</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
        <li><a href="{{ route('user_role.index') }}"><i class="fa fa-cogs"></i> User Roles</a></li>
        <li class="active">User Role Create</li>
    </ol>
</section>
<section class="content">
    <div class="container box">
        @if(!isset($user_role->id))
        <div class="box-header">
            <h3 class="box-title">User Role Basic Information</h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['user_role.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
        @else
        <div class="box-header">
            <h3 class="box-title">Edit User Role Basic Information of :: <b>{{ $user_role->display_name }} </b></h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['user_role.update',$user_role->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                {!! Form::hidden('_method','PUT') !!}
        @endif
            <div class='form-group row'>
                {{Form::label('name','Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('name', $user_role->name ,['class' => 'form-control','placeholder'=>'Name','required'=> 'true'])}}
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('display_name','Dispaly Name',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('display_name', $user_role->display_name ,['class' => 'form-control','placeholder'=>'Display Name','required'=> 'true'])}}
                    @if ($errors->has('display_name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('display_name') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>
        </div>               
    </div>            
</section>

<section class="content">
    <div class="container box">
        @if(!isset($user_role->id))
        <div class="box-header">
            <h3 class="box-title">Add Permissions</h3>
        </div>
        <div class="box-body">
        @else
        <div class="box-header">
            <h3 class="box-title">Edit Permissions</h3>
        </div>
        <div class="box-body">
        @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Permissions
                    </label>
                    <div class="col-sm-10">
                        @if(count($module_permissions) > 0)
                        <fieldset id="access_rights">
                            <div style="color:red">Check the boxes below to grant access to modules</div>

                            <dl id="permission_list">                                
                            <div class="col-sm-12"> 
                            @foreach($modules as $module)                               
                            <div class="col-sm-3">
                                <dt><span class="large">{{$module->module_name}}</span></dt>
                                @foreach($module_permissions as $module_permission)
                                    @if($module->module_name === $module_permission->module_name)
                                    <dd style="list-style: none;padding: 0px 10px;">
                                        <label style="width: 100%">
                                        @php
                                            $value='';
                                            if($user_role->hasPermission($module_permission)){
                                                $value='checked';
                                            }
                                        @endphp
                                            <input type="checkbox" @php echo $value; @endphp name="module_permission[]" value="{{ $module_permission->id }}">
                                            <span class="small">{{$module_permission->permission}}</span>
                                        </label>
                                    </dd>
                                    @endif
                                @endforeach
                            </div>
                            @endforeach                              
                            </div>
                            </dl>
                        </fieldset>
                    </div>
                        @else
                            <div class="alert alert-danger"> No Records Found ! </div>
                        @endif
                </div>
            </div>               
    </div>            
</section>

<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',['class' => 'btn btn-default'])}}
            {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#settings").addClass("active");
</script>

@endsection