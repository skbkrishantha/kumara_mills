@extends('layouts.print')

@section('title', 'Receipt #' . $supply->id )

@section('content')

    <header>
        <div style="position:absolute; left:0pt; width:250pt;">
            <img src="{{ public_path('/storage/images/logo.png') }}" class="img-rounded" height="60px">
        </div>
        <div style="margin-left:300pt;">
            <b>Date: </b> {{ $supply->date->format('Y-m-d') }}<br />
            <b>Ref #: </b> {{ $supply->id }}<br />
        </div>
        <br />
        <h2>{{ 'Supply Receipt #' . $supply->id }}</h2>
    </header>

    <main>
        <div style="clear:both; position:relative;">
            <div style="position:absolute; left:0pt; width:250pt;">
                <h4>Business Details:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ Auth::user()->company()->name }}<br />
                        {{ Auth::user()->company()->address }}<br />                        
                        Email: {{ Auth::user()->company()->email }}<br />
                        Telephone: {{ Auth::user()->company()->telephone }}<br />
                    </div>
                </div>
            </div>
            <div style="margin-left: 300pt;">
                <h4>Supplier Details:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ $supply->supplier->name  }}<br />
                        {{ $supply->supplier->address }}<br />                        
                        Phone: {{ $supply->supplier->phone01 }}<br />
                    </div>
                </div>
            </div>
        </div>
        <h4>Items:</h4>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Item Name</th>
                    <th align="center">Amount</th>
                    <th align="center">Unit</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @php ($i = 1)
                @foreach($carts as $cart)
                <tr>
                    <td>{{ $i++  }}</td>
                    <td>{{$cart->id}}</td>
                    <td>{{$cart->product->name}}</td>
                    <td align="right">{{$cart->units}}</td>
                    <td align="center">{{$cart->product->productUnit->display_name}}</td>                     
                    <td align="right">{{\App\Util\CommonFunc::getPrefixCurrencyFormat($cart->unit_price)}}</td>
                    <td align="right">{{\App\Util\CommonFunc::getPrefixCurrencyFormat($cart->units * $cart->unit_price)}}</td>                  
                </tr>
                @endforeach 
            </tbody>
        </table>
        <div style="clear:both; position:relative;">
            <div style="position:absolute; left:0pt; top:120pt; width:250pt;">
                <div class="panel panel-default" style="border-radius:0px;border-style: dotted;border-color:#000000">
                    <div class="panel-body">
                        
                    </div>
                </div>                
                <h4 align="center" style="padding-top:5px;">Signature:</h4>
            </div>
            <div style="margin-left: 300pt;">
                <h4></h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td><b>TOTAL</b></td>
                            <td align="right"><b>
                                {{\App\Util\CommonFunc::getPrefixCurrencyFormat($supply->total())}}                           
                            </b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <script type="text/php">
        if (isset($pdf) && $GLOBALS['with_pagination'] && $PAGE_COUNT > 1) {
            $pageText = "{PAGE_NUM} of {PAGE_COUNT}";
            $pdf->page_text(($pdf->get_width()/2) - (strlen($pageText) / 2), $pdf->get_height()-20, $pageText, $fontMetrics->get_font("DejaVu Sans, Arial, Helvetica, sans-serif", "normal"), 7, array(0,0,0));
        }
    </script>

@endsection