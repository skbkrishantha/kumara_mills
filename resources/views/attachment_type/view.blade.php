@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Attachment  Types
            <small>Handle all Attachment  Types resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li><a href="{{ route('attachment_type.index') }}"><i class="fa fa-cogs"></i>Attachment  Types</a></li>
            <li class="active">Attachment  Types View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Attachment  Types Deatails of :: <b>{{ $attachment_type->name }} </b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('attachment_type.edit', ['id' => $attachment_type->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $attachment_type->name }}</td>
                                </tr>
                                <tr>
                                    <td>Dispaly Name: </td>
                                    <td>{{ $attachment_type->display_name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection