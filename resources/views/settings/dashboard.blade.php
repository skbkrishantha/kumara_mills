@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Settings
            <small>Handle all Settings</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Settings</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box"> 
            <div class="col-md-4" style="padding: 10px;"> 
                <a href="{{ route('user_role.index') }}" class="material-main-select-blue">
                    <ol>
                        <li>User Role</li>
                    </ol>
                </a>
            </div>
            <div class="col-md-4" style="padding: 10px;"> 
                <a href="{{ route('module_permission.index') }}" class="material-main-select-green">
                    <ol>
                        <li>Module Permission</li>
                    </ol>
                </a>
            </div>
            <div class="col-md-4" style="padding: 10px;"> 
                <a href="{{ route('identity_card_type.index') }}" class="material-main-select-orange">
                    <ol>
                        <li>Identity Card Type</li>
                    </ol>
                </a>
            </div>
            <div class="col-md-4" style="padding: 10px;"> 
                <a href="{{ route('product_unit.index') }}" class="material-main-select-red">
                    <ol>
                        <li>Product Unit</li>
                    </ol>
                </a>
            </div>
            
            <div class="col-md-4" style="padding: 10px;"> 
                <a href="{{ route('salary_period.index') }}" class="material-main-select-yellow">
                    <ol>
                        <li>Salary Period</li>
                    </ol>
                </a>
            </div>
           
        </div>
    </section>   
    

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection