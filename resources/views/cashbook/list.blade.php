@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                CashBook
                <small>Handle all CashBook Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('cashbook.index') }}"><i class="fa fa-smile-o"></i> CashBook</a></li>  
                <li class="active">List CashBook</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">CashBook List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'cashbook.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    
                    <!--
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>
                    -->

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($cashbooks) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Reference</th>
                        <th>Amount</th>
                        <th>Payment Type</th>
                        <th>Module Type</th>
                        <th>Transaction Type</th>
                        <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($cashbooks as $cashbook)
                    <tr>
                        <td>{{ (($cashbooks->currentpage()-1) * $cashbooks->perpage()) + $i++  }}</td>
                        <td>{{$cashbook->id}}</td>
                        <td>{{$cashbook->amount}}</td>
                        <td>{{$cashbook->payment_type}}</td>
                        <td>{{$cashbook->module_type}}</td>
                        <td>{{$cashbook->transaction_type}}</td>
                        <td>{{$cashbook->update_datetime}}</td>                      
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $cashbooks->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#cashbook").addClass("active");
</script>
@endsection