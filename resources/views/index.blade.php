@extends('layouts.app')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><i class="fa fa-dashboard"></i> Dashboard</a></li>      
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    
    <!-- Small boxes (Stat box) -->
    <div class="row">    
      <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ App\Model\Client::ClientCount() }}</h3>
              <p>Clients</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ route('client.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ App\Model\Supplier::SupplierCount()}}</h3>

              <p>Suppliers</p>
            </div>
            <div class="icon">
              <i class="fa fa-truck"></i>
            </div>
            <a href="{{ route('supplier.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ App\Model\Product::count() }}</h3>

              <p>Products</p>
            </div>
            <div class="icon">
              <i class="fa fa-product-hunt"></i>
            </div>
            <a href="{{ route('product.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ App\Model\Employee::count()}}</h3>

              <p>Employees</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="{{ route('employee.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

      </div>


      <div class="col-md-12" style="padding:0px">      
        <div class="col-md-12"  style="padding:0px">
            <!-- Application buttons -->
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Quick Access</h3>
              </div>
              <div class="box-body">
                <a class="btn btn-app" href="{{ route('supply.create') }}">
                  <i class="fa fa-plus"></i> Add Supply
                </a>
                <a class="btn btn-app" href="{{ route('supply.index') }}">
                  <i class="fa fa fa-arrow-right"></i> Supply List
                </a>
                <a class="btn btn-app" href="{{ route('sell.create') }}">
                  <i class="fa fa-plus"></i> Add Sell
                </a>
                <a class="btn btn-app" href="{{ route('sell.index') }}">
                  <i class="fa fa-arrow-left"></i> Sell List
                </a>
                <a class="btn btn-app" href="{{ route('stock.dashboard') }}">
                  <i class="fa fa-cubes"></i> Stock
                </a>
                <a class="btn btn-app" href="{{ route('cashbook.index') }}">
                  <i class="fa fa-money"></i> Cashbook
                </a>
                <a class="btn btn-app" href="{{ route('product.index') }}">
                  <i class="fa fa-product-hunt"></i> Product
                </a>
                <a class="btn btn-app" href="{{ route('client.create') }}">
                  <i class="fa fa-plus"></i> Add Client
                </a>
                <a class="btn btn-app" href="{{ route('client.index') }}">
                  <i class="fa fa-smile-o"></i> Clients
                </a>
                <a class="btn btn-app" href="{{ route('supplier.create') }}">
                  <i class="fa fa-plus"></i> Add Supplier
                </a>
                <a class="btn btn-app" href="{{ route('supplier.index') }}">
                  <i class="fa fa-truck"></i> Suppliers
                </a>
                <a class="btn btn-app" href="{{ route('salary.index') }}">
                  <i class="fa fa-usd"></i> Salary
                </a>
                <!--<a class="btn btn-app" href="{{ route('employee.create') }}">
                  <i class="fa fa-plus"></i> Add Employee
                </a>-->
                <a class="btn btn-app" href="{{ route('employee.index') }}">
                  <i class="fa fa-users"></i> Employees
                </a>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-12 nav-tabs-custom">
          <!-- Tabs within a box -->
          <ul class="nav nav-tabs pull-right">
            <li class="pull-left header"><i class="fa fa-inbox"></i>Current Stock</li>
          </ul>
          <div class="tab-content no-padding">
            <!-- Morris chart - Sales -->
            <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
          </div>
        </div>
      </div>

      <div class="col-md-6" style="padding:0px 0px 0px 0px">
          <!-- Application buttons -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Event Calander</h3>
            </div>
            <div class="box-body" id="calander_div">

            </div>
          </div>
      </div>

  </section>
  <!-- /.content -->
  <script type="text/javascript">
    $("#dashboard").addClass("active");
    
    $.ajax({
      url: "{{ route('event.calander_view') }}",
      dataType: "html",
      success: function(html) {
          $("#calander_div").html(html);
      }
    });
 </script>
@endsection

@section('js')
  <script type="text/javascript" src="{{ URL::asset('dist/js/pages/dashboard.js') }}"></script>
@endsection