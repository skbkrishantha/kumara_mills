@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Salary Periods
            <small>Handle all Salary Periods resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li class="active">Salary Periods</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Salary Period List</h3> 
                <div class="semi pull-right">
                    <a href="{{ route('salary_period.create') }}" class="btn btn-success btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Add New
                    </a>
                </div>  
            </div>            
            <div class="box-body">
                {!! Form::open(['route' => 'salary_period.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                        {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($salary_periods) > 0)
                <div class="table-responsive" style="overflow: auto">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($salary_periods as $salary_period)
                        <tr>
                            <td>{{ (($salary_periods->currentpage()-1) * $salary_periods->perpage()) + $i++  }}</td>
                            <td>{{$salary_period->name}}</td>
                            <td>{{$salary_period->display_name}}</td>
                            <td title="{{ $salary_period->recordBy()}}">
                                <a href="{{ route('salary_period.show', ['id' => $salary_period->id]) }}" class="btn btn-info btn-xs">
                                    <i class="fa fa-info-circle"></i> View
                                </a>
                                <a href="{{ route('salary_period.edit', ['id' => $salary_period->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                    <i class="fa fa-pencil-square-o"></i> Edit
                                </a>
                            </td>
                        </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
                {{ $salary_periods->appends(Request::except('page'))->links() }}
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection