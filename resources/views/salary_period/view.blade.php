@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Salary Periods
            <small>Handle all Salary Periods resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i> Settings</a></li>
            <li><a href="{{ route('salary_period.index') }}"><i class="fa fa-cogs"></i> Salary Periods</a></li>
            <li class="active">Salary Period View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Salary Period Deatails of :: <b>{{ $salary_period->name }} </b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('salary_period.edit', ['id' => $salary_period->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $salary_period->name }}</td>
                                </tr>
                                <tr>
                                    <td>Dispaly Name: </td>
                                    <td>{{ $salary_period->display_name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    

<script type="text/javascript">
    $("#settings").addClass("active");
</script>
@endsection