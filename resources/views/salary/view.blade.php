@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Salary
            <small>Handle all Salary resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('salary.index') }}"><i class="fa fa-cogs"></i> Salary</a></li>
            <li class="active">Salary View</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Salary Deatails of :: <b>{{ $salary->employee->name }} for {{ $salary->salaryPeriod->display_name }}</b></h3>
                <div class="semi pull-right">
                    <a href="{{ route('salary.edit', ['id' => $salary->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                    <a href="{{ route('salary.print', ['id' => $salary->id]) }}" class="btn btn-primary btn-xs" Target="_blank">
                        <i class="fa fa-print "></i> Print
                    </a>
                </div>    
            </div>
            <div class="box-body"> 
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $salary->employee->name }}</td>
                                </tr>
                                <tr>
                                    <td>Dispaly Name: </td>
                                    <td>{{ $salary->salaryPeriod->display_name }}</td>
                                </tr>
                                <tr>
                                    <td>Basic Salary: </td>
                                    <td>{{ \App\Util\CommonFunc::getPrefixCurrencyFormat($salary->basic_salary) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                                                         
                                <tr>
                                    <td>Deductions: </td>
                                    <td>{{ \App\Util\CommonFunc::getPrefixCurrencyFormat($salary->total_deductions) }}</td>
                                </tr>
                                <tr>
                                    <td>Net Salary: </td>
                                    <td>{{ \App\Util\CommonFunc::getPrefixCurrencyFormat($salary->net_salary) }}</td>
                                </tr>                                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section>   
    

<script type="text/javascript">
    $("#salaries").addClass("active");
</script>
@endsection