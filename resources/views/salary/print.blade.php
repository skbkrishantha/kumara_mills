@extends('layouts.print')

@section('title', 'Payslip #' . $salary->id )

@section('content')

    <header>
        <div style="position:absolute; left:0pt; width:250pt;">
            <img src="{{ public_path('/storage/images/logo.png') }}" class="img-rounded" height="60px">
        </div>
        <div style="margin-left:300pt;">
            <b>Period: </b> {{ $salary->salaryPeriod->display_name }}<br />
            <b>Ref #: </b> {{ $salary->id }}<br />
        </div>
        <br />
        <h2>{{ 'Payslip #' . $salary->id }}</h2>
    </header>

    <main>
        <div style="clear:both; position:relative;">
            <div style="position:absolute; left:0pt; width:250pt;">
                <h4>Business Details:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ Auth::user()->company()->name }}<br />
                        {{ Auth::user()->company()->address }}<br />                        
                        Email: {{ Auth::user()->company()->email }}<br />
                        Telephone: {{ Auth::user()->company()->telephone }}<br />
                    </div>
                </div>
            </div>
            <div style="margin-left: 300pt;height:160px;">
            </div>
        </div>
        <h4><b>Payslip of {{ $salary->employee->name. ' ( ' . $salary->employee->employee_number }} ) for {{ $salary->salaryPeriod->display_name }}</b></h4>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan=2 >Earnings</th>
                    <th colspan=2 >Deductions</th>
                    <th colspan=2 >Calculations</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Basic Salary</td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->basic_salary) }}</td>
                    <td>EPF (Employee Contr.)</td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->epf_employee_contrib) }}</td>
                    <td>Total for EPF, ETF</td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->total_for_epf_etf) }}</td>
                </tr>
                <tr>
                    <td><b>Total Earnings</b></td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->total_earnings) }}</td>
                    <td><b>Total Deductions<b></td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->total_deductions) }}</td>
                    <td>EPF (Employee Contr. {{ $salary->epf_employee_contrib_precentage}}%)</td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->epf_employee_contrib) }}</td>
                </tr>
                <tr>
                    <td colspan=4 style="border:none;"></td>
                    <td>EPF (Company Contr. {{ $salary->epf_company_contrib_precentage}}%) </td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->epf_company_contrib) }}</td>
                </tr>
                <tr>
                    <td colspan=4 style="border:none;"></td>
                    <td>ETF (Company Contr. {{ $salary->etf_company_contrib_precentage}}%) </td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->etf_company_contrib) }}</td>
                </tr>
                <tr>
                    <td colspan=4 style="border:none;"></td>
                    <td>Total for Tax </td>
                    <td align=right>{{ \App\Util\CommonFunc::getCurrencyFormat($salary->total_for_epf_etf) }}</td>
                </tr>
                <tr>
                    <td colspan=6><b>Net Salary:</b> {{ \App\Util\CommonFunc::getPrefixCurrencyFormat($salary->net_salary) }}</td>
                </tr>

            </tbody>
        </table>
    </main>

    <script type="text/php">
        if (isset($pdf) && $GLOBALS['with_pagination'] && $PAGE_COUNT > 1) {
            $pageText = "{PAGE_NUM} of {PAGE_COUNT}";
            $pdf->page_text(($pdf->get_width()/2) - (strlen($pageText) / 2), $pdf->get_height()-20, $pageText, $fontMetrics->get_font("DejaVu Sans, Arial, Helvetica, sans-serif", "normal"), 7, array(0,0,0));
        }
    </script>

@endsection