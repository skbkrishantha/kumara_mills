@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Salary
            <small>Handle all Salary resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('salary.index') }}"><i class="fa fa-users"></i> Salary</a></li>
            <li class="active">Salary List</li>
        </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Salary List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'salary.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($salaries) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Salary Period</th>
                        <th>Basic Salary</th>
                        <th>Deductions</th>
                        <th>Net Salary</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($salaries as $salary)
                    <tr>
                        <td>{{ (($salaries->currentpage()-1) * $salaries->perpage()) + $i++  }}</td>
                        <td>{{$salary->employee->name}}</td>
                        <td>{{$salary->salaryPeriod->display_name }} </td>
                        <td>{{\App\Util\CommonFunc::getPrefixCurrencyFormat($salary->basic_salary)}}</td>
                        <td>{{\App\Util\CommonFunc::getPrefixCurrencyFormat($salary->total_deductions)}}</td>
                        <td>{{\App\Util\CommonFunc::getPrefixCurrencyFormat($salary->net_salary)}}</td>
                        <td title="{{ $salary->recordBy()}}">
                            <a href="{{ route('salary.show', ['id' => $salary->id]) }}" class="btn btn-info btn-xs">
                                <i class="fa fa-info-circle"></i> View
                            </a>
                            <a href="{{ route('salary.edit', ['id' => $salary->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </a>
                        </td>                       
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $salaries->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#salaries").addClass("active");
</script>
@endsection