@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Salary
        <small>Handle all Salary resources</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('salary.index') }}"><i class="fa fa-cogs"></i> Salary</a></li>
        <li class="active">Salary Create</li>
    </ol>
</section>
<section class="content">
    <div class="container box">
        @if(!isset($salary->id))
        <div class="box-header">
            <h3 class="box-title">Salary Basic Information</h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['salary.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
        @else
        <div class="box-header">
            <h3 class="box-title">Edit Salary Basic Information of :: <b>{{ $salary->display_name }} </b></h3>
        </div>
        <div class="box-body">
                {!! Form::open(['route' => ['salary.update',$salary->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                {!! Form::hidden('_method','PUT') !!}
        @endif

            <div class="form-group row">
                {{Form::label('employee_id','Employee',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {!!Form::select('employee_id', $employee_list, $salary->employee_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                    @if ($errors->has('employee_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('employee_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{Form::label('salary_period_id','Salary Period',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {!!Form::select('salary_period_id', $salary_period_list, $salary->salary_period_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                    @if ($errors->has('salary_period_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('salary_period_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class='form-group row'>
                {{Form::label('epf_employee_contrib_precentage','EPF Employee Contribution Precentage',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('epf_employee_contrib_precentage', ($salary->epf_employee_contrib_precentage != null) ? $salary->epf_employee_contrib_precentage : $epf_employee_contrib_precentage->value ,['class' => 'form-control','placeholder'=>'EPF Employee Contribution Precentage','required'=> 'true'])}}
                    @if ($errors->has('epf_employee_contrib_precentage'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('epf_employee_contrib_precentage') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('epf_company_contrib_precentage','EPF Company Contribution Precentage',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('epf_company_contrib_precentage', ($salary->epf_company_contrib_precentage != null) ? $salary->epf_company_contrib_precentage : $epf_company_contrib_precentage->value ,['class' => 'form-control','placeholder'=>'EPF Company Contribution Precentage','required'=> 'true'])}}
                    @if ($errors->has('epf_company_contrib_precentage'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('epf_company_contrib_precentage') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>
            
            <div class='form-group row'>
                {{Form::label('etf_company_contrib_precentage','ETF Company Contribution Precentage',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-8">
                    {{Form::text('etf_company_contrib_precentage',($salary->etf_company_contrib_precentage != null) ? $salary->etf_company_contrib_precentage : $etf_company_contrib_precentage->value ,['class' => 'form-control','placeholder'=>'ETF Company Contribution Precentage','required'=> 'true'])}}
                    @if ($errors->has('etf_company_contrib_precentage'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('etf_company_contrib_precentage') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>

            <div class='form-group row'>
                {{Form::label('basic_salary','Basic Salary',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                <div class="col-md-3">
                    <div class="input-group">                            
                        <span class="input-group-addon">Rs.</span>                           
                        {{Form::text('basic_salary', $salary->basic_salary  ,[  'class' => 'form-control currency-fommater','required'=> 'true'])}}
                    </div>
                    @if ($errors->has('basic_salary'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('basic_salary') }}</strong>
                        </span>
                    @endif
                </div> 
            </div>
            

            <div class='form-group row'>
                    {{Form::label('comment','Comment',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $salary->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

        </div>               
    </div>            
</section>

<section class="content">
    <div class="container box">
            @if(!isset($cashbook->id))
            <div class="box-header">
                <h3 class="box-title">CashBook Basic Information</h3>
            </div>
            @else
            <div class="box-header">
                <h3 class="box-title">Edit CashBook Basic Information of :: <b>{{ $cashbook->id }} </b></h3>
            </div>
            @endif
            
            <div class="box-body">

                <!--<div class='form-group row'>
                    {{Form::label('amount','Amount',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group">                            
                            <span class="input-group-addon">Rs.</span>                           
                            {{Form::text('amount', $cashbook->amount  ,['class' => 'form-control currency-fommater','required'=> 'true'])}}
                        </div>
                        @if ($errors->has('amount'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>-->

                <div class="form-group row">
                    {{Form::label('payment_type','Payment Type',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('payment_type', App\Type\PaymentType::getKeyValue(), $cashbook->payment_type, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('payment_type'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('payment_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class='form-group row'>
                    {{Form::label('effective_date','Effective Date',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group date" data-date-format="yyyy-mm-dd" name="datetimepicker">
                            {{Form::text('effective_date', $cashbook->effective_date  ,[  'class' => 'form-control','required'=> 'true','placeholder' => 'Effective Date','readonly'])}} 
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        
                        @if ($errors->has('effective_date'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('effective_date') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('reference_no','Reference Number',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('reference_no', $cashbook->reference_no ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Reference Number'])}}
                        @if ($errors->has('reference_no'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('reference_no') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('cashbook_comment','Comment',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('cashbook_comment', $cashbook->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('cashbook_comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

            </div>
    </div>            
</section>

<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',['class' => 'btn btn-default'])}}
            {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#salaries").addClass("active");
    $('div[name ="datetimepicker"]').datetimepicker({
        format:'yyyy-mm-dd',
        pickTime: false,
        minView: 2,
        autoclose: 1,
    });
</script>

@endsection