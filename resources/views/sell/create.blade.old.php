@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Sell
            <small>Handle all Sell Resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('sell.index') }}"><i class="fa fa-smile-o"></i> Sell</a></li>            
            @if(!isset($sell->id))
                <li class="active">Add Sell</li>
            @else   
                <li class="active">Edit Sell</li>
            @endif     
        </ol>
</section>
@if(!isset($sell->id))
    {!! Form::open(['id' => 'sellForm' , 'route' => ['sell.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
@else   
    {!! Form::open(['id' => 'sellForm' , 'route' => ['sell.update',$sell->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
    {!! Form::hidden('_method','PUT') !!}
@endif 
<section class="content">
    <div class="container box">
            @if(!isset($sell->id))
            <div class="box-header">
                <h3 class="box-title">Sell Basic Information</h3>
            </div>                   
            @else
            <div class="box-header">
                <h3 class="box-title">Edit Sell Basic Information of :: <b>{{ $sell->id }} </b></h3>
            </div>                   
            @endif

            <div class="box-body">
            
                <div class='form-group row'>
                    {{Form::label('date','Sell Date',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group date" data-date-format="yyyy-mm-dd" id="datetimepicker3">
                            {{Form::text('date', (!$sell->date) ? $sell->date : $sell->date->format('Y-m-d')  ,['class' => 'form-control','required'=> 'true','placeholder' => 'Sell Date','readonly'])}}
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        
                        @if ($errors->has('date'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class="form-group row">
                    {{Form::label('client_id','Client',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('client_id', $client_list, $sell->client_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('client_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('client_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {{Form::label('employee_id','Employee',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('employee_id', $employee_list, $sell->employee_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('employee_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('employee_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
          
                <div class='form-group row'>
                    {{Form::label('comment','Comment',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $sell->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

            </div>
    </div>            
</section>

<section class="content">
    <div class="container box">
            @if(!isset($stock->id))
            <div class="box-header">
                <h3 class="box-title">Stock Basic Information</h3>
            </div>
            @else
            <div class="box-header">
                <h3 class="box-title">Edit Stock Basic Information of :: <b>{{ $stock->id }} </b></h3>
            </div>
            @endif
            
            
            <div class="box-body">
                <div class="col-12">
                    <table class="table row-adder" id="stockDetails">
                        <tbody>
                            <tr class="defaultRow d-flex">
                                <td class="col-5">
                                    {!!Form::select('product_id', $product_list, $stocks[0]->product_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                                </td>
                                <td class="col-6">                                    
                                    {{Form::number('units', $stocks[0]->units ,['class' => 'form-control', 'min' => '0', 'placeholder'=>'Units','required'=> 'true'])}}
                                    {{Form::text('stock_units','0.000',['id' => 'stock_units', 'disabled' => 'true','placeholder'=>'# Units in the stock', 'class' => 'form-control'])}}
                                </td>
                                <td class="col-6">
                                    {{Form::text('stock_units','0.000',['id' => 'stock_units', 'disabled' => 'true','placeholder'=>'# Units in the stock', 'class' => 'form-control'])}}
                                </td>
                                <td class="col-1"><button type="button"
                                        class="ibtnDel btn btn-danger btn-xs d-none">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                            <c:forEach begin="1" end="${holder.stocks.size()-1}" varStatus="i">
                                <tr class="d-flex">
                                    <td class="col-5">
                                        <form:select path="stocks[${i.index}].idType.id" class="form-control required">
                                            <form:option value="" label="--- Select ---" disabled="disabled"/>
                                            <form:options items="${idTypes}" itemLabel="name" itemValue="id"/>
                                        </form:select>
                                    </td>
                                    <td class="col-6">
                                        <form:input type="text" class="form-control required id-number" path="stocks[${i.index}].id_number" />
                                    </td>
                                    <td class="col-1"><button type="button"
                                            class="ibtnDel btn btn-danger btn-xs">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <tbody>
                            <tr>
                                <td><button type="button"
                                        class="addrow btn btn-info btn-xs">
                                        <i class="fa fa-plus"></i> Add ID Number
                                    </button>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>												
                </div>
										

                <div class='form-group row'>
                    {{Form::label('comment','Comment',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $stock->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

            </div>
    </div>            
</section>

<section class="content">
    <div class="container box">
            @if(!isset($cashbook->id))
            <div class="box-header">
                <h3 class="box-title">CashBook Basic Information</h3>
            </div>
            @else
            <div class="box-header">
                <h3 class="box-title">Edit CashBook Basic Information of :: <b>{{ $cashbook->id }} </b></h3>
            </div>
            @endif
            
            <div class="box-body">

                <div class='form-group row'>
                    {{Form::label('amount','Amount',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group">                            
                            <span class="input-group-addon">Rs.</span>                           
                            {{Form::text('amount', $cashbook->amount  ,['class' => 'form-control currency-fommater','required'=> 'true'])}}
                        </div>
                        @if ($errors->has('amount'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class="form-group row">
                    {{Form::label('payment_type','Payment Type',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('payment_type', App\Type\PaymentType::getKeyValue(), $cashbook->payment_type, ['placeholder' => 'Please select ...','class' => 'form-control', 'required'=> 'true'])!!}
                        @if ($errors->has('payment_type'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('payment_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!--
                <div class="form-group row">
                    {{Form::label('transaction_type','Transction Type',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('transaction_type', App\Type\TransactionType::getKeyValue(), $cashbook->transaction_type, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('transaction_type'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('transaction_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                -->

                <div class='form-group row'>
                    {{Form::label('comment','Comment',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $cashbook->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

            </div>
    </div>            
</section>


<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',['class' => 'btn btn-default'])}}
            {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>
{!! Form::close() !!}

<script type="text/javascript">
    $("#sell").addClass("active");

    $('#datetimepicker3').datetimepicker({
        format:'yyyy-mm-dd',
        pickTime: false,
        minView: 2,
        autoclose: 1,
    });

    
    $('#product_id').on('change', function (e) {
        $.ajax({
            type:'GET',
            url:'/product/get_unit_by_id/'+ this.value ,
            success: function( msg ) {
                var obj = JSON.parse(JSON.stringify(msg))
                $("#product_unit_id").val(obj.id);
            }
        });
    });

    setStockUnits($('#product_id').val());

    $('#product_id').on('change', function (e) {
        setStockUnits(this.value);
    });


    function setStockUnits($product_id){
        $.ajax({
            type:'GET',
            url:'/stock/get_stock_by_product_id/'+ $product_id,
            success: function( msg ) {
                var obj = JSON.parse(JSON.stringify(msg))
                var stock_units;
                $("#product_unit_id").val(obj.product_unit_id);
                if(obj.units){
                    stock_units = obj.units;
                }else{
                    stock_units = "No";
                }
                $("#stock_units").val(stock_units+" units in the stock");
            }
        });
    }

</script>
@endsection