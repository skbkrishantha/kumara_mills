@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Sell
                <small>Handle all Sell Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('sell.index') }}"><i class="fa fa-smile-o"></i> Sell</a></li>  
                <li class="active">List Sell</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Sell Deatails of :: <b>{{ $sell->id }} </b> </h3>
                <div class="semi pull-right">
                    <a href="{{ route('sell.edit', ['id' => $sell->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                        <i class="fa fa-pencil-square-o"></i> Edit
                    </a>                    
                    <a href="{{ route('sell.print', ['id' => $sell->id]) }}" class="btn btn-primary btn-xs" Target="_blank">
                        <i class="fa fa-print "></i> Print
                    </a>
                </div>    
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>                                            
                                <tr>
                                    <td>Reference No: </td>
                                    <td>{{ $sell->id }}</td>
                                </tr>
                                <tr>
                                    <td>date: </td>
                                    <td>{{ $sell->date }}</td>
                                </tr>
                                <tr>
                                    <td>Employee: </td>
                                    <td>{{ $sell->employee->name }}</td>
                                </tr>
                                <tr>
                                    <td>Client: </td>
                                    <td>{{ $sell->client->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-striped simple">
                            <tbody>
                                <tr>
                                    <td>Comment: </td>
                                    <td>{{ $sell->comment }}</td>
                                </tr>                                             
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </section> 
    
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Stock Deatails </h3>  
            </div>
            <div class="box-body">
                @php ($i = 1)
                @if(count($carts) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Reference</th>
                                <th>Product Id</th>
                                <th>Units</th>
                                <th>Unit</th>
                                <th>Stock Type</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($carts as $cart)
                        <tr>
                            <td>{{ $i++  }}</td>
                            <td>{{$cart->id}}</td>
                            <td>{{$cart->product->name}}</td>
                            <td>{{$cart->units}}</td>
                            <td>{{$cart->product->productUnit->display_name}}</td>
                            <td>{{$cart->stock_type}}</td>                    
                        </tr>
                        @endforeach    
                        </tbody>
                    </table>
                </div>
                @else
                    <div class="alert alert-danger"> No Records Found ! </div>
                @endif
            </div> 
        </div>
    </section> 
    
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">CashBook Deatails </h3>  
            </div>
            <div class="box-body">
                @php ($i = 1)
                    @if(count($cashbooks) > 0)
                    <!-- /.box-header -->
                    <div class="table-responsive" style="overflow: auto">
                    <table id="dataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Reference</th>
                                <th>Amount</th>
                                <th>Payment Type</th>
                                <th>Transaction Type</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($cashbooks as $cashbook)
                        <tr>
                            <td>{{ $i++  }}</td>
                            <td>{{$cashbook->id}}</td>
                            <td>{{$cashbook->amount}}</td>
                            <td>{{$cashbook->payment_type}}</td>
                            <td>{{$cashbook->transaction_type}}</td>                   
                        </tr>
                        @endforeach    
                        </tbody>
                    </table>
                    </div>
                @else
                    <div class="alert alert-danger"> No Records Found ! </div>
                @endif
            </div> 
        </div>
    </section> 

<script type="text/javascript">
    $("#sell").addClass("active");
</script>
@endsection