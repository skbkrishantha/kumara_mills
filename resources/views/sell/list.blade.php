@extends('layouts.app')

@section('content')
    <section class="content-header">
            <h1>
                Sell
                <small>Handle all Sell Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{ route('sell.index') }}"><i class="fa fa-smile-o"></i> Sell</a></li>  
                <li class="active">List Sell</li>    
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Sell List</h3>   
            </div>            
            <div class="box-body">
                <!-- form start -->
                {!! Form::open(['route' => 'sell.index','method' => 'GET','enctype' => 'multipart/form-data']) !!}
                    <!--
                    {{Form::label('search-name','Name',['class' => 'col-sm-1 control-label'])}}
                    
                    <div class="col-sm-3">
                        <div class="input-group">
                            {{Form::text('search-name', $request->input('search-name') ,['class' => 'form-control','placeholder'=>'Name'])}}
                            <span id="btnTextClear" class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                        <script>
                            $(".glyphicon-remove").click(function(){
                                $("#search-name").val('');
                            });
                        </script>
                    </div>
                    -->  

                    {{Form::label('search-status','Status',['class' => 'col-sm-1 control-label'])}}

                    <div class="col-sm-2">
                        {!!Form::select('search-status',App\Type\StatusType::getKeys(), $request->input('search-status'), ['class' => 'form-control'])!!}
                    </div>

                    <div class="col-sm-1">
                    {{Form::submit('Search',['class' => 'btn btn-info center-block'])}}
                    </div>
                {!! Form::close() !!}
                </div>
                @php ($i = 1)
                @if(count($sells) > 0)
                <!-- /.box-header -->
                <div class="table-responsive" style="overflow: auto">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Client</th>
                        <th>employee</th> 
                        <th>Update Time</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($sells as $sell)
                    <tr>
                        <td>{{ (($sells->currentpage()-1) * $sells->perpage()) + $i++  }}</td>
                        <td>{{$sell->date->format('Y-m-d')}}</td>
                        <td>{{$sell->client->name}}</td>
                        <td>{{$sell->employee->name}}</td>
                        <td>{{$sell->update_datetime}}</td>
                        <td title="{{ $sell->recordBy()}}">
                            <a href="{{ route('sell.show', ['id' => $sell->id]) }}" class="btn btn-info btn-xs">
                                <i class="fa fa-info-circle"></i> View
                            </a>
                            <a href="{{ route('sell.edit', ['id' => $sell->id]) }}" class="btn btn-warning btn-xs" data-toggle="confirmation">
                                <i class="fa fa-pencil-square-o"></i> Edit
                            </a>
                        </td>                       
                    </tr>
                    @endforeach    
                    </tbody>
                </table>
                </div>
            <div class="centerBlock">
                {{ $sells->appends(Request::except('page'))->links() }}
            </div>
            @else
                <div class="alert alert-danger"> No Records Found ! </div>
            @endif
        </div>
    </section>

<script type="text/javascript">
    $("#sell").addClass("active");
</script>
@endsection