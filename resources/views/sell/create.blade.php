@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Sell
            <small>Handle all Sell Resources</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('sell.index') }}"><i class="fa fa-smile-o"></i> Sell</a></li>            
            @if(!isset($sell->id))
                <li class="active">Add Sell</li>
            @else   
                <li class="active">Edit Sell</li>
            @endif     
        </ol>
</section>

{!! Form::open(['id' => 'stockOutForm', 'route' => ['stock.out_add_to_cart'], 'method' => 'get','enctype' => 'multipart/form-data']) !!}
    
<section class="content">
    <div class="container box">
            @if(!isset($stock->id))
            <div class="box-header">
                <h3 class="box-title">Stock Basic Information</h3>
            </div>
            @else
            <div class="box-header">
                <h3 class="box-title">Edit Stock Basic Information of :: <b>{{ $stock->id }} </b></h3>
            </div>
            @endif
            <div class="box-body">                
                <div class="form-row">
                    <div class="form-group col-md-3">
                        {{Form::label('product_id','Product',[  'class' => ''])}}
                        {!!Form::select('product_id', $product_list,'', ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('product_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('product_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        {{Form::label('units','Units',[  'class' => ''])}}                
                        {{Form::number('units','' ,[  'class' => 'form-control', 'min' => '0', 'placeholder'=>'Units','required'=> 'true'])}}
                        @if ($errors->has('units'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('units') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>                 

                <div class="form-row">
                    <div class="form-group col-md-2">
                        {{Form::label('product_unit_id','Product Unit',[  'class' => ''])}}
                        {!!Form::select('product_unit_id', $product_unit_list,'', ['placeholder' => 'Please select ...','class' => 'form-control','disabled' => 'true', 'required'=> 'true'])!!}
                        @if ($errors->has('product_unit_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('product_unit_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>                

                <div class='form-row'>
                    <div class="form-group col-md-2">                    
                        {{Form::label('unit_price','Unit Price',['class' => ''])}}
                        <div class="input-group">                            
                            <span class="input-group-addon">Rs.</span>                           
                            {{Form::text('unit_price','',['class' => 'form-control currency-fommater','required'=> 'true'])}}
                        </div>
                        @if ($errors->has('unit_price'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('unit_price') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        {{Form::label('stock_units','Available Stock',[  'class' => ''])}}  
                        {{Form::text('stock_units','0.000',['id' => 'stock_units', 'disabled' => 'true','placeholder'=>'# Units in the stock', 'class' => 'form-control'])}}
                    </div>
                </div> 

                <div class="form-row">
                    <div class="form-group col-md-2">
                        {{Form::label('action','Actions',[  'class' => ''])}} 
                        {{Form::button('Add',[ 'type' => 'submit', 'class' => 'btn btn-primary form-control ' ])}}
                    </div>
                </div> 

            </div>

            <div id="cart"></div>
    </div>            
</section>

{!! Form::close() !!}


@if(!isset($sell->id))
    {!! Form::open(['id' => 'sellForm' , 'route' => ['sell.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
@else   
    {!! Form::open(['id' => 'sellForm' , 'route' => ['sell.update',$sell->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
    {!! Form::hidden('_method','PUT') !!}
@endif 

<section class="content">
    <div class="container box">    
            <div class="box-header">
                @if(!isset($sell->id))
                    <h3 class="box-title">Sell Basic Information</h3>                 
                @else
                    <h3 class="box-title">Edit Sell Basic Information of :: <b>{{ $sell->id }} </b></h3>                
                @endif
            <button type="button" class="btn btn-primary pull-right" onclick="pageRefresh()"><i class="fa fa-refresh"></i> Refresh</button>                 
            </div>  

            <div class="box-body">
            
                <div class='form-group row'>
                    {{Form::label('date','Sell Date',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group date" data-date-format="yyyy-mm-dd" name="datetimepicker">
                            {{Form::text('date', (!$sell->date) ? $sell->date : $sell->date->format('Y-m-d')  ,[  'class' => 'form-control','required'=> 'true','placeholder' => 'Sell Date','readonly'])}}
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        
                        @if ($errors->has('date'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class="form-group row">
                    {{Form::label('client_id','Client',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('client_id', $client_list, $sell->client_id, [ 'placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('client_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('client_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {{Form::label('employee_id','Employee',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('employee_id', $employee_list, $sell->employee_id, ['placeholder' => 'Please select ...','class' => 'form-control','required'=> 'true'])!!}
                        @if ($errors->has('employee_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('employee_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
          
                <div class='form-group row'>
                    {{Form::label('comment','Comment',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $sell->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

            </div>
    </div>            
</section>

<section class="content">
    <div class="container box">
            @if(!isset($cashbook->id))
            <div class="box-header">
                <h3 class="box-title">CashBook Basic Information</h3>
            </div>
            @else
            <div class="box-header">
                <h3 class="box-title">Edit CashBook Basic Information of :: <b>{{ $cashbook->id }} </b></h3>
            </div>
            @endif
            
            <div class="box-body">

                <div class='form-group row'>
                    {{Form::label('amount','Amount',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group">                            
                            <span class="input-group-addon">Rs.</span>                           
                            {{Form::text('amount', $cashbook->amount  ,[  'class' => 'form-control currency-fommater','required'=> 'true'])}}
                        </div>
                        @if ($errors->has('amount'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class="form-group row">
                    {{Form::label('payment_type','Payment Type',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {!!Form::select('payment_type', App\Type\PaymentType::getKeyValue(), $cashbook->payment_type, ['placeholder' => 'Please select ...','class' => 'form-control', 'required'=> 'true'])!!}
                        @if ($errors->has('payment_type'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('payment_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class='form-group row'>
                    {{Form::label('effective_date','Effective Date',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-3">
                        <div class="input-group date" data-date-format="yyyy-mm-dd" name="datetimepicker">
                            {{Form::text('effective_date', $cashbook->effective_date  ,[  'class' => 'form-control','required'=> 'true','placeholder' => 'Effective Date','readonly'])}} 
                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        
                        @if ($errors->has('effective_date'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('effective_date') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('reference_no','Reference Number',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::text('reference_no', $cashbook->reference_no ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Reference Number'])}}
                        @if ($errors->has('reference_no'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('reference_no') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

                <div class='form-group row'>
                    {{Form::label('comment','Comment',[  'class' => 'col-sm-2 col-form-label text-md-right'])}}
                    <div class="col-md-8">
                        {{Form::textarea('comment', $cashbook->comment ,['id'=>'article-ckeditor','class' => 'form-control','placeholder'=>'Comment','rows'=>4])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div> 
                </div>

            </div>
    </div>            
</section>


<div class="row">
    <div class="form-group">
        <div class="col-sm-12 col-sm-offset-1">
            {{Form::reset('Reset',[  'class' => 'btn btn-default'])}}
            {{Form::submit('Submit',[  'class' => 'btn btn-primary'])}}
        </div>
    </div>    
</div>
<br>
<br>


{!! Form::close() !!}

<script type="text/javascript">
    $("#sell").addClass("active");

    var default_date=  new Date();
    $('div[name ="datetimepicker"]').datetimepicker({
        format:'yyyy-mm-dd',
        pickTime: false,
        minView: 2,
        autoclose: 1,
        language: 'en',
        defaultDate: default_date
    });

    
    $('#product_id').on('change', function (e) {
        setProductUnit(this.value);
    });

    setStockUnits($('#product_id').val());

    $('#product_id').on('change', function (e) {
        setStockUnits(this.value);
    });

    function setProductUnit($product_id){
        $.ajax({
            type:'GET',
            url:'/product/get_unit_by_id/'+ $product_id ,
            data:{},
            success: function( msg ) {
                var obj = JSON.parse(JSON.stringify(msg))
                $("#product_unit_id").val(obj.id);
            }
        });
    }

    function setStockUnits($product_id){
        $.ajax({
            type:'GET',
            url:'/stock/get_stock_by_product_id/'+ $product_id,
            data:{},
            success: function( msg ) {
                var obj = JSON.parse(JSON.stringify(msg))
                var stock_units;
                $("#product_unit_id").val(obj.product_unit_id);
                $("#unit_price").val(obj.unit_price);
                if(obj.units){
                    stock_units = obj.units;
                }else{
                    stock_units = "No";
                }
                $("#stock_units").val(stock_units+" units in the stock");
            }
        });
    }

    function pageRefresh(){

            var currentURL = window.location.href;
            $.ajax({
                type:'GET',
                url: currentURL,
                data:{skip:0},
                success: function( data ) {
                    var clients = $('#client_id').append(data).find('#client_id');
                    var employees = $('#employee_id').append(data).find('#employee_id');
                    $('#client_id').html(clients.html());
                    $('#employee_id').html(employees.html());
                    
                }
            });
    }
 

</script>
@endsection

@section('js')  
    <script type="text/javascript" src="{{ URL::asset('js/cart-item-remover.js') }}"></script>
@endsection