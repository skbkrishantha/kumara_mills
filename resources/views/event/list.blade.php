@extends('layouts.app')

<link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet">

@section('content')
    <script type="text/javascript">
        eventRender: function(event, element) {
            element.html(event.title + '<span class="removeEvent glyphicon glyphicon-trash pull-right" id="Delete"></span>');
        }
    </script>
    <section class="content-header">
            <h1>
                Event
                <small>Handle all Event Resources</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('index') }}"><i class="fa fa-dashboard"></i> Home</a></li> 
                <li class="active">Event</li>  
            </ol>
    </section>
    <section class="content">
        <div class="container box">
            <div class="box-header">
                <h3 class="box-title">Event Calander</h3>
                <div class="semi pull-right">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#eventModel">
                        <i class="fa fa-print "></i> Add Event
                    </button>
                </div>  
            </div>
            <div class="box-body">
                <div class="col-md-6" id="event-calander">
                    <div class="panel panel-primary">
                        <div class="panel-body" sytle="background-color: #eee;">
                            {!! $calendar->calendar() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Model -->
    <div class="modal fade" id="eventModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Event</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => ['event.store'],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
                        <div class='form-group row'>
                            {{Form::label('title','Title',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                            <div class="col-md-8">
                                {{Form::text('title',null,['class' => 'form-control','placeholder'=>'Title','required'=> 'true'])}}
                            </div>
                        </div>
                        <div class='form-group row'>
                            {{Form::label('from','From',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                            <div class="col-md-8">
                                <div class="input-group date" data-date-format="yyyy-mm-dd" id="datetimepicker3">
                                    {{Form::text('from',null,['class' => 'form-control','required'=> 'true','placeholder' => 'From','readonly'])}}
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                </div>
                            </div> 
                        </div>
                        <div class='form-group row'>
                            {{Form::label('to','To',['class' => 'col-sm-2 col-form-label text-md-right'])}}
                            <div class="col-md-8">
                                <div class="input-group date" data-date-format="yyyy-mm-dd" id="datetimepicker4">
                                    {{Form::text('to',null,['class' => 'form-control','required'=> 'true','placeholder' => 'To','readonly'])}}
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="form-group text-center">
                                <div class="col-sm-12">
                                    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
                                </div>
                            </div> 
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>    

<script type="text/javascript">
    $("#event").addClass("active");    
</script>


<script type="text/javascript" src="{{ URL::asset('js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/fullcalendar.min.js') }}"></script>
{!! $calendar->script() !!}

<script type="text/javascript">
    $('#datetimepicker3').datetimepicker({
        format:'yyyy-mm-dd',
        pickTime: false,
        minView: 2,
        autoclose: 1,
    });

    $('#datetimepicker4').datetimepicker({
        format:'yyyy-mm-dd',
        pickTime: false,
        minView: 2,
        autoclose: 1,
    });
</script>

@endsection