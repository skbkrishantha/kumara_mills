<html>
    <head>
        
        <link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet">        
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    </head>    
    <body>
            <div class="col-md-12" id="event-calander">
                <div class="panel panel-primary">
                    <div class="panel-body" sytle="background-color: #eee;">
                        {!! $calendar->calendar() !!}
                    </div>
                </div>
            </div>

        <script type="text/javascript" src="{{ URL::asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/fullcalendar.min.js') }}"></script>
        {!! $calendar->script() !!}
    </body>
</html>
