<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_employee', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('name',255);
            $table->datetime('joined_date');
            $table->string('employee_number',255)->nullable();
            $table->string('epf_number',255)->nullable();
            $table->string('address_01',255);
            $table->string('address_02',255)->nullable();
            $table->string('identity_card_number',32);
            $table->string('telephone',20)->nullable();
            $table->string('mobile1',20);
            $table->string('mobile2',20)->nullable();
            $table->string('fax',20)->nullable();
            $table->string('email',128)->nullable();
            $table->string('web',128)->nullable();
            $table->decimal('basic_salary', 15, 2)->nullable();
            $table->text('comment')->nullable(); 
            $table->tinyInteger('status');
            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();
            $table->string('user_account_id', 32);
            $table->string('identity_card_type_id', 32);
            $table->foreign('user_account_id')->references('id')->on('tbl_user_account');
            $table->foreign('identity_card_type_id')->references('id')->on('mst_identity_card_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_employee');
    }
}
