<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_supplier', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('name',255);
            $table->string('identity_card_number',255);
            $table->string('email',255)->nullable();
            $table->string('mobile1',20);
            $table->string('mobile2',20)->nullable();
            $table->text('address1');
            $table->text('address2')->nullable();
            $table->datetime('joined_date');
            $table->text('comment')->nullable(); 
            $table->tinyInteger('status');
            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();
            $table->string('identity_card_type_id', 32);
            $table->foreign('identity_card_type_id')->references('id')->on('mst_identity_card_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_supplier');
    }
}
