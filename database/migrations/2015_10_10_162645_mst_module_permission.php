<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstModulePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('mst_module_permission', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('module_name',255);
            $table->string('permission',255);
            $table->tinyInteger('status');
            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_module_permission');
    }
}
