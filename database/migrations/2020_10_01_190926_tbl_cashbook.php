<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCashbook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_cashbook', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->decimal('amount',20,2); 
            $table->String('payment_type',255);            
            $table->String('module_type',255);  
            $table->String('transaction_type',255);  
            $table->String('reference_no',255)->nullable();
            $table->date('effective_date');   

            $table->text('comment')->nullable(); 
            $table->tinyInteger('status');            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();

            $table->string('reference_id', 32);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_cashbook');
    }
}
