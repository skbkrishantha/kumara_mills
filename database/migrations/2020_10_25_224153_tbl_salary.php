<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_salary', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->datetime('date');              
            $table->decimal('basic_salary',20,2);     
            $table->decimal('allowances',20,2);             
            $table->decimal('total_for_epf_etf',20,2);    
            $table->decimal('epf_employee_contrib',20,2);  
            $table->decimal('epf_company_contrib',20,2);               
            $table->decimal('etf_company_contrib',20,2);             
            $table->decimal('epf_employee_contrib_precentage',20,2);  
            $table->decimal('epf_company_contrib_precentage',20,2);               
            $table->decimal('etf_company_contrib_precentage',20,2);                          
            $table->decimal('total_earnings',20,2);                         
            $table->decimal('total_deductions',20,2);                          
            $table->decimal('total_for_tax',20,2);                         
            $table->decimal('net_salary',20,2);  

            $table->text('comment')->nullable(); 
            $table->tinyInteger('status');            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();

            $table->string('employee_id', 32);
            $table->foreign('employee_id')->references('id')->on('tbl_employee'); 
            
            $table->string('salary_period_id', 32);
            $table->foreign('salary_period_id')->references('id')->on('mst_salary_periods'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_salary');
    }
}
