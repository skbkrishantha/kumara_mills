<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblEmployeeAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_employee_attachments', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('path',1000);
            $table->string('comment',255)->nullable();
            $table->tinyInteger('status');
            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();
            $table->string('employee_id', 32);
            $table->string('attachment_types_id', 32);
            $table->foreign('employee_id')->references('id')->on('tbl_employee');
            $table->foreign('attachment_types_id')->references('id')->on('mst_attachment_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_employee_attachments');
    }
}
