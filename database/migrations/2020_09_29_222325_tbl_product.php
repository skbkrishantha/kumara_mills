<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('name',255);
            $table->string('display_name',255);   
            $table->string('current_price',255);          
            $table->string('material_type',255)->nullable();

            $table->tinyInteger('status');            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();   
                     
            $table->string('product_unit_id', 32);
            $table->foreign('product_unit_id')->references('id')->on('mst_product_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product');
    }
}
