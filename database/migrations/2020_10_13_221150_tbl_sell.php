<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSell extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sell', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->datetime('date');    

            $table->text('comment')->nullable(); 
            $table->tinyInteger('status');            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();

            $table->string('client_id', 32);
            $table->foreign('client_id')->references('id')->on('tbl_client');
            $table->string('employee_id', 32);
            $table->foreign('employee_id')->references('id')->on('tbl_employee'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sell');
    }
}
