<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_stock', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->decimal('units',20,3);
            $table->decimal('unit_price',20,3);                        
            $table->String('module_type',255);
            $table->string('stock_type',255);     

            $table->text('comment')->nullable(); 
            $table->tinyInteger('status');            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable();

            $table->string('product_id', 32);
            $table->foreign('product_id')->references('id')->on('tbl_product');                  
            $table->string('product_unit_id', 32);
            $table->foreign('product_unit_id')->references('id')->on('mst_product_units');            
            $table->string('reference_id', 32);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_stock');
    }
}
