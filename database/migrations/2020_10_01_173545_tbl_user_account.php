<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblUserAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user_account', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('email',255)->unique();
            $table->decimal('lockout_time', 64, 0)->nullable();
            $table->string('user_password',255);
            $table->string('user_name',255);
            $table->string('remember_token',100)->nullable();
            $table->tinyInteger('status');
            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable(); 
            $table->string('role_id', 32);
            $table->foreign('role_id')->references('id')->on('mst_user_role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user_account');
    }
}
