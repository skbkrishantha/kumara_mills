<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_company', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('name',255);
            $table->string('address',1000);
            $table->string('telephone',20);
            $table->string('hotline',20);
            $table->string('mobile',20)->nullable();
            $table->string('fax',20)->nullable();
            $table->string('email',128);
            $table->string('web',128)->nullable();
            $table->string('logo_path',1000)->nullable();
            $table->string('comment',255)->nullable();
            $table->tinyInteger('status');
            
            $table->string('insert_user_id', 32)->nullable(); 
            $table->datetime('insert_datetime')->nullable(); 
            $table->string('update_user_id', 32)->nullable();
            $table->datetime('update_datetime')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_company');
    }
}
