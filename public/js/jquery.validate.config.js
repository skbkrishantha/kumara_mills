  		

$(function(){
		
	$.validator.setDefaults({
	    highlight: function(element) {
	        jQuery(element).closest('.form-control').addClass('is-invalid');
	    },
	    unhighlight: function(element) {
	        jQuery(element).closest('.form-control').removeClass('is-invalid');
	        jQuery(element).closest('.form-control').addClass('is-valid');
	    },
	    errorElement: 'div',
	    errorClass: 'invalid-feedback',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
				error.insertAfter(element.parent());
	        } else {
				error.insertAfter(element);
	        }
	    }
	});
	
	$.validator.addMethod("regx", function(value, element, regexpr) {
	    return this.optional(element) || regexpr.test(value);
	}, "Input is invalid.");
	
	$.validator.addMethod("sl-phone", function(value, element, regexpr) {
	    return this.optional(element) || regexpr.test(value);
	}, "invalid phone number.{format ex: 0xxxxxxxxx}");
	
	$.validator.addMethod("id-number", function(value, element, regexpr) {
	    return this.optional(element) || regexpr.test(value);
	}, "invalid ID number.");
	
	$.validator.addClassRules('phone', {
		"sl-phone" : /^((0)[0-9]{9})$/,
	});
	
	$.validator.addClassRules('id-number', {
		"id-number" : /^([0-9]{9}(v|V)|[0-9]{12}|(PBLKA)[A-Z0-9]{8,9}|[0-9]{10})$/,
	});

		
	$("#sellForm").validate({
		rules : {},
		submitHandler : function(form) {
			$('#product_unit_id').prop("disabled", false);
			$('#stock_units').prop("disabled", false);
			form.submit();
		}
	});

	$("#supplyForm").validate({
		rules : {},
		submitHandler : function(form) {
			$('#product_unit_id').prop("disabled", false);
			$('#stock_units').prop("disabled", false);
			form.submit();
		}
	});

	$('#stockOutForm').submit(function(e) {	
		e.preventDefault();
	}).validate({
		rules: {
			product_id: {
				required: true
			},
			units: {
				required: true,
				number: true,
				min: 0,
				remote: {
					url: "/stock/stock_exists",
					type: 'GET',
					data: { product_id: function(){ return $('#product_id').val()} },
					delay: 1000,
				},			 
			},
			product_unit_id: {
				required: true
			},
			stock_units: {
				required: true
			},
			subject: {
				required: true
			}
		},

		messages : {
			"units": {
            	remote: "Stock not enough!",
        	},			
		},

		submitHandler: function(form) {			
			$('#product_unit_id').prop("disabled", false);
			var form_btn = $(form).find('button[type="submit"]');
			var form_btn_old_msg = form_btn.html();
			form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
			$(form).ajaxSubmit({
				success: function(data) {
					$(form).find('.form-control').val('');
					$("#cart").html(data);
					$amount = $(data).find("#cart_total").text();
					$('input[name="amount"]').val($amount);
					form_btn.prop('disabled', false).html(form_btn_old_msg);
					setStockUnits($('#product_id').val());
				}
			});
			$('#product_unit_id').prop("disabled", true);	
		}
	});

	$('#stockInForm').submit(function(e) {	
		e.preventDefault();
	}).validate({
		rules: {
			product_id: {
				required: true
			},
			units: {
				required: true,
				number: true,
				min: 0			 
			},
			product_unit_id: {
				required: true
			},
			stock_units: {
				required: true
			},
			unit_price: {
				required: true
			},
			subject: {
				required: true
			}
		},

		submitHandler: function(form) {			
			$('#product_unit_id').prop("disabled", false);
			var form_btn = $(form).find('button[type="submit"]');
			var form_btn_old_msg = form_btn.html();
			form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
			$(form).ajaxSubmit({
				success: function(data) {
					$(form).find('.form-control').val('');
					$("#cart").html(data);
					$amount = $(data).find("#cart_total").text();
					$('input[name="amount"]').val($amount);
					form_btn.prop('disabled', false).html(form_btn_old_msg);
					setStockUnits($('#product_id').val());
				}				
			});			
			$('#product_unit_id').prop("disabled", true);
		}
	});


	
});
