$(document).ready(function () {

	$.ajax({
		type:'GET',
		url:'/stock/remove_from_cart/'+ this.id,
		data:{},
		success: function( data ) {
			$("#cart").html(data);
			$amount = $(data).find("#cart_total").text();
			$('input[name="amount"]').val($amount);
			setStockUnits($('#product_id').val());
		}
	});
	
    $(this).on("click", ".ibtnDel", function (event) {		
		$.ajax({
            type:'GET',
            url:'/stock/remove_from_cart/'+ this.id,
            data:{},
            success: function( data ) {
				$("#cart").html(data);
				$amount = $(data).find("#cart_total").text();
				$('input[name="amount"]').val($amount);
				setStockUnits($('#product_id').val());
            }
        });

    });


});