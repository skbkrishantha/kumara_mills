$('[data-toggle=confirmation]').confirmation();
$('[data-toggle=confirmation-singleton]').confirmation({ singleton:true });
$('[data-toggle=confirmation-popout]').confirmation({ popout: true });

function readURL(input,id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(id)
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
} 

$('input.currency-fommater').maskNumber();

























